import ACTION from "../actions/actionTypes"

const initialState = {
    user: {},
    isAuthenticated: false,
    loginStatus: 'ideal',
    resetpasswordStatus:'ideal',
    forgotpasswordStatus:'ideal'
}

const authReducer = (state = initialState, action) => {
    // console.log(action)
    switch(action.type){
        case ACTION.SET_LOGGEDIN_USER : 
            return {
                ...state,
                isAuthenticated: true,
                user: action.payload
            }

        case ACTION.LOGIN_CRED_REQUEST : 
            return {
                ...state,
                loginStatus: 'requesting'
            }

        case ACTION.LOGIN_CRED_SUCCESS : 
            return {
                ...state,
                loginStatus: 'success'
            } 

        case ACTION.LOGIN_CRED_FAILURE : 
            return {
                ...state,
                loginStatus: 'failure'
            }
            
        case ACTION.FOROTPASSWORD_CRED_REQUEST : 
            return {
                ...state,
                forgotpasswordStatus: 'requesting'
            }

        case ACTION.FOROTPASSWORD_CRED_SUCCESS : 
            return {
                ...state,
                forgotpasswordStatus: 'success'
            } 

        case ACTION.FOROTPASSWORD_CRED_FAILURE : 
            return {
                ...state,
                forgotpasswordStatus: 'failure'
            }
        
        case ACTION.RESETPASSWORD_CRED_REQUEST : 
            return {
                ...state,
                resetpasswordStatus: 'requesting'
            }

        case ACTION.RESETPASSWORD_CRED_SUCCESS : 
            return {
                ...state,
                resetpasswordStatus: 'success'
            } 

        case ACTION.RESETPASSWORD_CRED_FAILURE : 
            return {
                ...state,
                resetpasswordStatus: 'failure'
            }
        
        case ACTION.LOGOUT: 
            return initialState    

        default: return state    
    }
}

export default authReducer