import ACTION from "./actionTypes";

export const loginRequest = () => {
  return {
    type: ACTION.LOGIN_CRED_REQUEST,
  };
};

export const loginRequestSuccess = () => {
  return {
    type: ACTION.LOGIN_CRED_SUCCESS,
  };
};

export const loginRequestFailure = () => {
  return {
    type: ACTION.LOGIN_CRED_FAILURE,
  };
};

export const setUser = (user) => {
  return {
    type: ACTION.SET_LOGGEDIN_USER,
    payload: user,
  };
};

export const logout = () => {
  return {
    type: ACTION.LOGOUT,
  };
};

export const forgotpasswordRequest = () => {
  return {
    type: ACTION.FOROTPASSWORD_CRED_REQUEST,
  };
};

export const forgotpasswordRequestSuccess = () => {
  return {
    type: ACTION.FOROTPASSWORD_CRED_SUCCESS,
  };
};

export const forgotpasswordRequestFailure = () => {
  return {
    type: ACTION.FOROTPASSWORD_CRED_FAILURE,
  };
};

export const resetpasswordRequest = () => {
  return {
    type: ACTION.RESETPASSWORD_CRED_REQUEST,
  };
};

export const resetpasswordRequestSuccess = () => {
  return {
    type: ACTION.RESETPASSWORD_CRED_SUCCESS,
  };
};

export const resetpasswordRequestFailure = () => {
  return {
    type: ACTION.RESETPASSWORD_CRED_FAILURE,
  };
};



export const studentRequestSuccess = (studentdata) => {
  return {
    type: ACTION.STUDENT_REQUEST_SUCCESS,
    payload: studentdata,
  };
};
export const StudentDeleteFailure = () => {
  return {
    type: ACTION.STUDENTDELETE_FAILURE,
    
  };
};
export const StudentDeleteSuccess = () => {
  return {
    type: ACTION.STUDENTDELETE_SUCCESS,
    
  };
};
export const StudentDeleteRequest = () => {
  return {
    type: ACTION.STUDENTDELETE_REQUEST,
  
  };
};

