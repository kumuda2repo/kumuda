import axios from "axios";
import make_API_call from "../../services/REST_API";
import firebase from "../../firebase_config";
import {
  setUser,
  loginRequest,
  loginRequestSuccess,
  loginRequestFailure,
  forgotpasswordRequest,
  forgotpasswordRequestSuccess,
  forgotpasswordRequestFailure,
  resetpasswordRequest,
  resetpasswordRequestSuccess,
  resetpasswordRequestFailure,
  logout,
} from "../../Authentication/actions/actionCreators";
import {
  waitingMsg,
  stopWaitMsg,
  errorMsg,
  successMsg,
} from "../../SharedComponents/AllSanckBars";
// import { backDrop } from "../../SharedComponents/AllSanckBars";
import history from "../../history";

const base_url = window.location.href.includes("http://localhost:3005")
  ? "http://localhost:5000/kumuda-test/us-central1/api"
  : "https://us-central1-kumuda-test.cloudfunctions.net/api";

export const _login = (email, password) => (dispatch) => {
  let userInfo;
  // uid
  dispatch(loginRequest());
  waitingMsg("Please Wait...");
  return (
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      // .then(({ user }) => {
      //   console.log(user);
      //   uid = user.uid;
      // firebase.auth().signOut();
      // return axios.post(`${base_url}/auth/login`, { uid: uid });
      // })
      // .then((res) => {
      //   const customToken = res.data.token;
      //   return firebase.auth().signInWithCustomToken(customToken);
      // })
      .then(({ user }) => {
        userInfo = {
          uid: user.uid,
          email,
        };
        dispatch(loginRequestSuccess());
        stopWaitMsg();
        return user.getIdToken();
      })
      .then((idToken) => {
        // assigning token
        console.log(idToken);
        axios.defaults.headers.common["Authorization"] = `Bearer ${idToken}`;
        axios.defaults.baseURL = base_url;
        dispatch(setUser(userInfo));
        return;
      })
      .catch((err) => {
        console.error(err);
        if (
          err.code === "auth/wrong-password" ||
          err.code === "auth/user-not-found"
        )
          alert("Incorrect username or password.");
        else alert(err.message);
        stopWaitMsg();
        return dispatch(loginRequestFailure());
      })
  );
};

export const _on_page_refresh = () => (dispatch) => {
  let userInfo;
  return firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      userInfo = {
        uid: user.uid,
        email: user.email,
      };
      user
        .getIdToken()
        .then((idToken) => {
          console.log(idToken);
          axios.defaults.headers.common["Authorization"] = `Bearer ${idToken}`;
          axios.defaults.baseURL = base_url;
          dispatch(setUser(userInfo));
          return;
        })
        .catch((err) => {
          console.error(err);
          return;
        });
    }
  });
};

export const _logout = () => (dispatch) => {
  return firebase
    .auth()
    .signOut()
    .then(() => {
      // removing token
      axios.defaults.headers.common["Authorization"] = ``;
      axios.defaults.baseURL = "";
      dispatch(logout());
      successMsg("Successfully Logged Out");
      history.push("/");
      window.location.reload();
      return;
    })
    .catch((err) => {
      console.error(err);
      console.log("logged out");
    });
};

export const _forgot_password = (email) => (dispatch) => {
  waitingMsg("Please Wait...");
  // console.log(email);
  dispatch(forgotpasswordRequest());
  return axios
    .post(`${base_url}/auth/forgotpassword`, { email: email })
    .then((response) => {
      // console.log(response);
      stopWaitMsg();
      successMsg("Check your inbox for a link to reset your password");
      dispatch(forgotpasswordRequestSuccess());
      // TODO: set status in store, call snackbar
    })
    .catch((err) => {
      // console.log(err);
      stopWaitMsg();
      errorMsg("No record of your EmailId in Kumuda");
      dispatch(forgotpasswordRequestFailure());
    });
};

export const _change_password = (password) => (dispatch) => {
  waitingMsg("Please Wait...");
  // console.log(password);
  dispatch(resetpasswordRequest());
  return make_API_call("put", "/auth/changepassword", { password })
    .then((response) => {
      // console.log(response);
      stopWaitMsg();
      successMsg("Password changed successfully!");
      dispatch(resetpasswordRequestSuccess());
      // TODO: set status in store, call snackbar
    })
    .catch((err) => {
      console.error(err);
      stopWaitMsg();
      errorMsg("Please re-login to change password");
      dispatch(resetpasswordRequestFailure());
    });
};

export const _create_student = (studentObj) => (dispatch) => {
  return make_API_call("post", "/auth/createstudent", studentObj)
    .then((response) => {
      // console.log(response);
      // TODO: set status in store, call snackbar
    })
    .catch((err) => {
      console.error(err);
    });
};

export const _update_student = (studentObj) => (dispatch) => {
  return make_API_call("put", "/auth/updatestudent", studentObj)
    .then((response) => {
      // console.log(response);
      // TODO: set status in store, call snackbar
    })
    .catch((err) => {
      console.error(err);
    });
};
