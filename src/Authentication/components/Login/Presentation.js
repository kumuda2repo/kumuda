import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Paper,
  FormControlLabel,
  Button,
} from "@material-ui/core";
//import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
//import { Visibility, VisibilityOff } from "@material-ui/icons";
import Forgotpassword from "../../../Authentication/components/Forgotpassword";
import kumudalogo from "../../../assets/kumudalogo.png";
import Sasi from "../../../assets/Sasi.png";
import "../../../Styles/login.css";
import {CustomField} from "../../../SharedComponents/FormFeilds";
const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiBottomNavigationAction-wrapper": {
      position: "relative",
    },
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
  },
}));

function Presentation(props) {
  const {
    handleChange,
    handleEmailChange,
    handlePasswordChange,
    handleClear,
    Enabled,
    Passwordenable,
    ClearEnable,
    handleLogin,
  } = props;
  const classes = useStyles();



  return (
    <div className="login" >
      <div className="login__main">
        <Paper className="login__card" elevation={0}>
          <div className="login__image">
            <img
              src={Sasi}
              style={{ width: "330px", height: "100px" }}
              alt="Sasi_Logo"
            />
          </div>
          <div className="login__form">
             {/* src\SharedComponents\FormFeilds.js*/}
              <CustomField
                className="login__mail"
                id="login_mail"
                variant="outlined"
                label="Email"
                value={props.emailId}
                handleChange={handleEmailChange}
                type="email"
                name="email"
                fullWidth
              />
              <CustomField
                className="login__password"
                id="login_password"
                label="Password"
                variant="outlined"
                size="small"
                type={props.showPassword ? "text" : "password"}
                name="Password"
                value={props.Password}
                handleChange={handlePasswordChange}
                disabled={!Passwordenable} 
                fullWidth
              />
              <FormControlLabel
                name="agreement"
                control={
                  <CustomField
                    type="checkbox"
                    value={props.checkedB}
                    checked={props.checkedB}
                    handleChange={handleChange}
                    name="checkedB"
                    id="login_checkbox"
                    color="primary"
                  />
                }
                label="Remember Me"
              />
              <div className="loginbuttons">
                <div className={classes.root} style={{ position: "relative" }}>
                  <Button
                    variant="contained"
                    color="primary"
                    disabled={!Enabled}
                    // href="/resetpassword"
                    onClick={handleLogin}
                    id="login_loginbutton"
                    type="submit"
                  >
                    Login
                  </Button>
                </div>
                <Button
                  className="capitalize"
                  onClick={handleClear}
                  disabled={!ClearEnable}
                  variant="contained"
                  id="login_clearbutton"
                  color="primary"
                >
                  Clear
                </Button>
              </div>
              <Forgotpassword />

           
          </div>
          <div className="login__footer">
            <span style={{ display: "flex", marginLeft: "25%" }}>
              <p style={{ marginTop: "15px" }}>Powered by</p>
              <span>
                <img
                  src={kumudalogo}
                  alt="Kumuda_logo"
                  style={{ height: "48px" }}
                />
              </span>
            </span>
          </div>
        </Paper>
        {/* {
        render(
          <Backdrop openBackdrop={propsdetails.loading === "requesting"} />,
          document.getElementById("backdrop-loading")
        )} */}
        {/* <Backdrop
          className={classes.backdrop}
          open={propsdetails.loading === "requesting"}
          onClick={handleClose1}
        >
          <CircularProgress color="inherit" />
        </Backdrop> */}
      </div>
    </div>
  );
}

export default Presentation;
