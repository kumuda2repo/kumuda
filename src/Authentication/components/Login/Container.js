import React, { Component } from "react";
import Presentation from "./Presentation";
import { connect } from "react-redux";
import { _login } from "../../middleware/middleware";

class Container extends Component {
  constructor() {
    super();
    this.state = {
      emailId: "",
      Password: "",
      check: false,
      error: "",
      showPassword: false,
      checkedB: true,
      open: false,
    };
  }

  handleEmailChange = (e) => {
    const emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    const checkemail = emailPattern.test(e.target.value);
    this.setState({
      emailId: e.target.value,
      check: checkemail,
    });
    if (checkemail === false && this.state.emailId.length > 1) {
      this.setState({
        error: "Invalid Email-Id",
      });
    } else {
      this.setState({
        error: " ",
      });
    }
  };
  handlePasswordChange = (e) => {
    this.setState({
      Password: e.target.value,
    });
    // console.log(e.target.value);
  };

  handleClear = () => {
    this.setState({
      emailId: "",
      Password: "",
    });
  };

  handleChange = (e) => {
    this.setState({
      [e.target.name]:e.target.value
    });
  };

  

  handleLogin = () => {
    // console.log(this.props);
    this.props.login(
      this.state.emailId,
      this.state.Password,
      this.props.history
    );
    this.setState({
      open: !this.state.open,
    });
  };

  

  render() {
    return (
      <div>
        <Presentation
          handleChange={this.handleChange}
          handleLogin={this.handleLogin}
          handleClickShowPassword={this.handleClickShowPassword}
          handleMouseDownPassword={this.handleMouseDownPassword}
          handleEmailChange={this.handleEmailChange}
          handlePasswordChange={this.handlePasswordChange}
          handleClear={this.handleClear}
          handleClose1={this.handleClose}
          propsdetails={this.props}
          {...this.state}
          Enabled={
            this.state.emailId.length >= 10 &&
            this.state.Password.length >= 8 &&
            this.state.check === true
          }
          Passwordenable={this.state.emailId.length >= 10}
          ClearEnable={
            this.state.emailId.length > 0 || this.state.Password.length > 0
          }
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  // console.log(state.auth.loginStatus);
  return {
    loading: state.auth.loginStatus,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    login: (email, password, history) =>
      dispatch(_login(email, password, history)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
