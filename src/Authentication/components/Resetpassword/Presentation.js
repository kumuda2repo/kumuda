import React from "react";
import { Button, Dialog, DialogContent, ListItemText } from "@material-ui/core";
//import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import "../../../Styles/resetPassword.css";
import { CustomField } from "../../../SharedComponents/FormFeilds";
function Presentation(props) {
  const {
    data,
    handleChangepassword,
    handleconfirmPassword,
    handlenewPassword,
    Enable,
    handleClick,
    handleClose,
  } = props;

  return (
    <div>
      <ListItemText id="resetpassword_button" onClick={handleClick}>
        Change password
      </ListItemText>

      <Dialog
        fullWidth
        maxWidth="sm"
        open={data.open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <h2 style={{ textAlign: "center", marginTop: "15px" }}>
          Change Password
        </h2>
        <DialogContent>
          <div className="resetpassword">
            <div className="resetpassword__main">
              <div className="resetpassword__form">
                 {/* src\SharedComponents\FormFeilds.js*/}
                <CustomField
                  className="resetpassword__password"
                  id="resetpassword_newpassword"
                  label="New Password"
                  size="small"
                  type="password"
                  name="Password"
                  value={data.newPassword}
                  handleChange={handlenewPassword}
                  helperText={
                    <span style={{ color: "red" }}>
                      {props.data.newPassword === "" ? (
                        ""
                      ) : (
                        <p>
                          <span style={{ color: "green" }}>
                            {" "}
                            {props.data.alertmsg}
                          </span>
                        </p>
                      )}
                    </span>
                  }
                  required
                  fullWidth
                />
                <CustomField
                  className="resetpassword__password"
                  id="resetpassword_confirmpassword"
                  label="Confirm Password"
                  size="small"
                  type="password"
                  name="Password"
                  value={data.confirmPassword}
                  handleChange={handleconfirmPassword}
                  helperText={
                    <span>
                      {data.confirmPassword === "" ? (
                        ""
                      ) : data.newPassword === data.confirmPassword ? (
                        <p style={{ color: "green" }}> Password match </p>
                      ) : (
                        <p style={{ color: "red" }}> Password not match </p>
                      )}
                    </span>
                  }
                  required
                  fullWidth
                />

                <span>
                  {data.newPassword.length <= 7 ? (
                    <>
                      <b>Make sure it's</b>{" "}
                      <b style={{ color: "red" }}>at least 8 characters</b>
                    </>
                  ) : (
                    <>
                      <b>Make sure it's</b>{" "}
                      <b style={{ color: "green" }}>at least 8 characters</b>
                    </>
                  )}
                </span>
                <br />
                <br />
                <div className="resetpasswordbuttons">
                  <Button
                    className="capitalize"
                    disabled={!Enable}
                    onClick={handleChangepassword}
                    variant="contained"
                    id="resetpassword_submit"
                    color="primary"
                  >
                    Submit
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </DialogContent>
      </Dialog>
    </div>
  );
}

export default Presentation;
