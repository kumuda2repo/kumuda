import React, { Component } from "react";
import Presentation from "./Presentation";
import { connect } from "react-redux";
import { _change_password } from "../../middleware/middleware";

class Container extends Component {
  constructor() {
    super();
    this.state = {
      newPassword: "",
      confirmPassword: "",
      Verifiedmsg: "",
      alertmsg: "",
      minmsg: "",
      open: false,
    };
  }

  handlenewPassword = (event) => {
    this.setState({
      newPassword: event.target.value,
    });
    if (this.state.newPassword.length <= 7) {
      this.setState({
        minmsg: "Password Must be Minimum 8 characters ",
      });
    }
    if (this.state.newPassword.length < 8 ) {
      this.setState({
        alertmsg: " Weak Password",
      });
    }
    if ((this.state.newPassword.match(/[a-zA-Z]+/) && this.state.newPassword.match(/[0-9]+/) && this.state.newPassword.length > 7 ) || (this.state.newPassword.length > 8)) {
      this.setState({
        alertmsg: "Moderate Password",
        minmsg: "",
      });
    }
    if (
      this.state.newPassword.match(/[!@£$%^&()]/) &&
      this.state.newPassword.length > 8
    ) {
      this.setState({
        alertmsg: "Strong Password",
        minmsg: "",
      });
    }
  };

  handleconfirmPassword = (event) => {
    this.setState({
      confirmPassword: event.target.value,
    });
  };

  handleChangepassword = (event) => {
    this.handleClose();
    this.props.resetpassword(this.state.confirmPassword);
    this.setState({
      newPassword: "",
      confirmPassword: "",
     });
  };

  handleClick = () => {
    this.setState({
      open: true,
    });
  };

  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({
      open: false,
    });
  };

  render() {
    return (
      <div>
        <div>
          <Presentation
            handleClick={this.handleClick}
            handleClose={this.handleClose}
            handlenewPassword={this.handlenewPassword}
            handleconfirmPassword={this.handleconfirmPassword}
            handleChangepassword={this.handleChangepassword}
            data={this.state}
            Enable={
              this.state.newPassword.length >= 8 &&
              this.state.newPassword.length ===
              this.state.confirmPassword.length
            }
          />
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    resetpassword: (conformPassword) =>
      dispatch(_change_password(conformPassword)),
  };
};

export default connect(null, mapDispatchToProps)(Container);
