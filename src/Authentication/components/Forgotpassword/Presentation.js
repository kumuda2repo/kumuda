import React from "react";
import {
  Button,
  Dialog,
  DialogContent,
  DialogContentText
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import "../../../Styles/forgotpassword.css";
import {CustomField} from "../../../SharedComponents/FormFeilds";
function Presentation(props) {
  const {
    Enabled,
    handleClick,
    handleClose,
    handleEmail,
    handleClickOpen,
    data,
  } = props;

  return (
<div>
      <Button className="text-primary" id="forgotpassword_button" onClick={handleClickOpen}>
                Forgot password?
      </Button>
    <Dialog
        open={data.open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
    >
    <DialogContentText style={{marginBottom:"5px",marginTop:"20px",textAlign:"justify",paddingRight:"60px",paddingLeft:"60px"}}>
                 Enter valid email address.
    </DialogContentText>
<DialogContent style={{marginTop:"none"}}>
    <div className="forgotpassword">
      <div className="forgotpassword__main">
        <div className="forgotpassword__form">
          <ValidatorForm>
          {/* src\SharedComponents\FormFeilds.js*/}
            <CustomField
              className="forgotpassword__mail"
              id="forgotpassword_mail"
              label="Email"
              size="small"
              value={data.email}
              handleChange={handleEmail}
              type="email"
              name="email"
              validators={["required", "isEmail"]}
              errorMessages={["this field is required", "email is not valid"]}
              fullWidth
              required
            />      
            <div className="forgotpasswordbuttons">
              <Button
                className="capitalize"
                onClick={handleClick}
                disabled={!Enabled}
                variant="contained"
                id="forgotpassword_button"
                color="primary"
              >
                Reset Password
              </Button>
            </div>
          </ValidatorForm>
        </div>
      </div>
    </div>
    </DialogContent>
  </Dialog>
</div>
  );
}

export default Presentation;
