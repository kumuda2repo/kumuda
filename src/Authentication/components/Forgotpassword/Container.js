import React, { Component } from "react";
import Presentation from "./Presentation";
import { _forgot_password } from "../../middleware/middleware";
import { connect } from "react-redux";

class Container extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      verifybtn: false,
      error: "",
      open: false,
    };
  }

  handleEmail = (e) => {
    const emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    const checkemail = emailPattern.test(e.target.value);
    this.setState({
      email: e.target.value,
      verifybtn: checkemail,
    });
    if (checkemail === false && this.state.email.length > 1) {
      this.setState({
        error: "Invalid Email-Id",
      });
    } else {
      this.setState({
        error: " ",
      });
    }
  };

handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({
      open: false,
    });
  };
handleClickOpen = () => {
    this.setState({
      open: true,
    });
}


handleClick = () => {
   this.handleClose();
   this.handleClearEmail();
    return this.props.forgotPassword(this.state.email);
    
};

handleClearEmail = () => {
  this.setState({
    email: "",
   });
}


  render() {
    // console.log(this.state);
    return (
      <div>
        <Presentation
          handleClick={this.handleClick}
          handleClose={this.handleClose}
          handleEmail={this.handleEmail}
          handleClicked={this.handleClicked}
          handleClickOpen={this.handleClickOpen}
          Enabled={this.state.email.length >= 10 && this.state.verifybtn}
          data={this.state}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    forgotPassword: (email) => dispatch(_forgot_password(email)),
  };
};

export default connect(null, mapDispatchToProps)(Container);
