import React from "react";
import MuiAlert from "@material-ui/lab/Alert";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function Presentation() {
  return (
    <div>
      <Alert severity="warning">Admissions Service is Coming Soon!.</Alert>
    </div>
  );
}

export default Presentation;
