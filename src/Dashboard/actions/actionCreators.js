import ACTION from "./actionTypes";

export const serviceRequest = (serviceName) => {
  return {
    type: ACTION.REQUEST_SERVICE,
    payload: serviceName
  };
};
