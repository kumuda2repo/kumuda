import { serviceRequest } from "../actions/actionCreators";

export const _service_request = (serviceName) => (dispatch) => {
  dispatch(serviceRequest(serviceName));
};

export const _set_value_in_store = (serviceName) => (dispatch) => {
  dispatch(serviceRequest(serviceName));
};
