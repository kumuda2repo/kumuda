import React, { Component } from "react";
import Presentation from "./Presentation";
import { connect } from "react-redux";
import { _logout } from "../../../Authentication/middleware/middleware";

class Container extends Component {
  handleLogout = () => {
    this.props.logout();
  };

  render() {
    return (
      <div>
        <Presentation handleLogout={this.handleLogout} />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => dispatch(_logout()),
  };
};

export default connect(null, mapDispatchToProps)(Container);
