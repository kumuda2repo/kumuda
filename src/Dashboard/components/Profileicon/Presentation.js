import React from "react";
import { Menu, MenuItem} from "@material-ui/core";
import sasilogo from "../../../assets/sasilogo.jpeg";
import Resetpassword from "../../../Authentication/components/Resetpassword";

function Presentation(props) {
  // console.log(props);
  const { handleLogout } = props;
  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
   
    <span>
      {/* <IconButton
        aria-label="account of current user"
        aria-controls="menu-appbar"
        aria-haspopup="true"
        onClick={handleMenu}
        color="inherit"
      > */}
      <img
        src={sasilogo}
        alt="sasilogo"
        style={{ width: "30px", borderRadius: "50%" }}
        aria-controls="customized-menu"
        aria-haspopup="true"
        onClick={handleClick}
      />
      {/* </IconButton> */}
      <Menu
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        id="menu-appbar"
        elevation={1}
        style={{ height: "90vh",marginTop:"20px" }}
        getContentAnchorEl={null}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
         
        }}
      // transformOrigin={{
      //   vertical: 'center',
      //   horizontal: 'right',
      // }}
      >
        <MenuItem onClick={handleClose} id="profileicon_profile">Profile</MenuItem>
        <MenuItem style={{ color: 'black'}} onClick={handleClose} id="profileicon_resetpassword" ><Resetpassword /></MenuItem>
        <MenuItem style={{ color: 'black'}} onClick={handleLogout} id="profileicon_logout">Log Out</MenuItem>
      </Menu>
    </span>
  );
}

export default Presentation;
