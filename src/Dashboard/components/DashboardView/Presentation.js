import React from "react";
import { Typography } from "@material-ui/core";
import Zoom from 'react-reveal/Zoom';
function Presentation() {
  return (
    <Zoom>
    <Typography variant="h4" gutterBottom style={{ textAlign: "center" }}>
      Welcome to KUMUDA!
    </Typography>
    </Zoom>
  );
}
export default Presentation;
