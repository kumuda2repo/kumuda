import React from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import {
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Divider,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import { SideNavbarItems } from "../../../SharedComponents/SideNavbarItems"

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap",
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(7) + 1,
    },
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
}));

function Presentation(props) {
  const {
    data,
    handleDrawerClose,
    handleDrawerOpen,
    currentLoadedService,
  } = props.data;
  const classes = useStyles();
   {/*src\SharedComponents\SideNavbarItems.js */}
  const sideBarItems = SideNavbarItems.filter(service => service.name === currentLoadedService)
  // console.log(sideBarItems[0].sideBarItems)
  return (
    <div>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: data.open,
          [classes.drawerClose]: !data.open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: data.open,
            [classes.drawerClose]: !data.open,
          }),
        }}
        onMouseOver={handleDrawerOpen}
        onMouseOut={handleDrawerClose}
      >
        <div className={classes.toolbar}></div>
        <Divider />
        <List>
          {currentLoadedService === ""
            ? null
            : sideBarItems[0].sideBarItems.map((item) => (
              <Link to={item.link}>
                <ListItem button key={item.itemText}>
                  <ListItemIcon>{item.itemIcon}</ListItemIcon>
                  <ListItemText primary={item.itemText} />
                </ListItem>
              </Link>
            ))
          }
        </List>
      </Drawer>
    </div>
  );
}

export default Presentation;
