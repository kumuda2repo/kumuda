import React, { Component } from "react";
import Presentation from "./Presentation";
import { connect } from "react-redux";

class Container extends Component {
  render() {
    // console.log(this.props);
    return (
      <div>
        <Presentation data={this.props} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentLoadedService: state.dashboard.currentLoadedService,
  };
};

export default connect(mapStateToProps)(Container);
