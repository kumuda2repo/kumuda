import React, { Component } from "react";
import Presentation from "./Presentation";
import { connect } from "react-redux";
import { _service_request } from "../../middleware/middleware";

class Container extends Component {
  handleRequest = (e) => {
    // console.log(e);
    this.props.serviceRequested(e);
  };

  render() {
    return (
      <span>
        <Presentation handleRequest={this.handleRequest} />
      </span>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    serviceRequested: (serviceName) => dispatch(_service_request(serviceName)),
  };
};

export default connect(null, mapDispatchToProps)(Container);
