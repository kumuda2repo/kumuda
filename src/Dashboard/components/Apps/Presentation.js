import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Popover } from "@material-ui/core";
import { CgMenuGridO } from "react-icons/cg";
import { RiUserSearchFill } from "react-icons/ri";
import { GiWallet } from "react-icons/gi";
import {
  FcGraduationCap,
  FcOvertime,
  FcEditImage,
  FcInspection,
  FcLeave,
  FcPortraitMode,
  FcSearch,
  FcStatistics,
  FcPodiumWithSpeaker,
  FcBriefcase,
  FcConferenceCall,
  FcSurvey,
  FcDocument,
  FcReadingEbook,
  FcCollaboration,
} from "react-icons/fc";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },

  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
    cursor: "pointer",
    '&:hover': {
      background: "#dadce0",
      borderRadius: "8px",
    },
  },

}));

function Presentation(props) {
  const { handleRequest } = props;
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const view = Boolean(anchorEl);
  const id = view ? "simple-popover" : undefined;

  return (
    <>
      <CgMenuGridO aria-describedby={id} size="25" onClick={handleClick} />
      <Popover
        id={id}
        open={view}
        anchorEl={anchorEl}
        style={{ height: "90vh",marginTop:"23px" }}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
        onClose={handleClose}
      >
        <div className="AppMenu">

          <div>
            <table>
              {/* Row1 */}
              <tr style={{ border: "1px solid gray"}}>
                <Link to="/admissions">
                  <td className={classes.paper} onClick={handleClose}>
                    <span>
                      <RiUserSearchFill size="25" />
                      <br />
                      <b>Admissions</b>
                    </span>
                  </td>
                </Link>
                <td className={classes.paper} onClick={handleClose}>
                  <span>
                    <FcGraduationCap size="25" />
                    <br />
                    <b>Academics</b>
                  </span>
                </td>
                <Link to="/scheduling">
                  <td className={classes.paper} onClick={() => { handleClose(); handleRequest("scheduling") }}>
                    <span>
                      <FcOvertime size="25" />
                      <br />
                      <b>Scheduling</b>
                    </span>
                  </td>
                </Link>
              </tr>
              {/* Row2 */}
              <tr>
                <td className={classes.paper} onClick={handleClose}>
                  <span>
                    <FcEditImage size="25" />
                    <br />
                    <b>Evaluation</b>
                  </span>
                </td>
                <td className={classes.paper} onClick={handleClose}>
                  <span>
                    <FcInspection size="25" />
                    <br />
                    <b>Assessments</b>
                  </span>
                </td>
                <td className={classes.paper} onClick={handleClose}>
                  <span>
                    <FcGraduationCap size="25" />
                    <br />
                    <b>Accreditation</b>
                  </span>
                </td>
              </tr>
              {/* Row3 */}
              <tr>
                <td className={classes.paper} onClick={handleClose}>
                  <span>
                    <FcLeave size="25" />
                    <br />
                    <b>Leaves</b>
                  </span>
                </td>
                <td className={classes.paper} onClick={handleClose}>
                  <span>
                    <FcPodiumWithSpeaker size="25" />
                    <br />
                    <b>MentorShip</b>
                  </span>
                </td>
                <td className={classes.paper} onClick={handleClose}>
                  <span>
                    <FcStatistics size="25" />
                    <br />
                    <b>Reports</b>
                  </span>
                </td>
              </tr>
              {/* Row4 */}
              <tr>
                <td className={classes.paper} onClick={handleClose}>
                  <span>
                    <FcSearch size="25" />
                    <br />
                    <b>Research</b>
                  </span>
                </td>
                <td className={classes.paper} onClick={handleClose}>
                  <span>
                    <FcPortraitMode size="25" />
                    <br />
                    <b>CRM</b>
                  </span>
                </td>
                <td className={classes.paper} onClick={handleClose}>
                  <span>
                    <GiWallet size="25" />
                    <br />
                    <b>Fee</b>
                  </span>
                </td>
              </tr>
              {/* Row5 */}
              <tr>
                <td className={classes.paper} onClick={handleClose}>
                  <span>
                    <FcBriefcase size="25" />
                    <br />
                    <b>Placement</b>
                  </span>
                </td>

                <td className={classes.paper} onClick={() => { handleClose(); handleRequest("users") }}>
                  <Link to="/users/studentslist">
                    <span>
                      <FcConferenceCall size="25" />
                      <br />
                      <b>Users</b>
                    </span>
                  </Link>
                </td>

                <td className={classes.paper} onClick={handleClose}>
                  <span>
                    <FcSurvey size="25" />
                    <br />
                    <b>Surveys</b>
                  </span>
                </td>
              </tr>
              {/* Row6 */}
              <tr>
                <td className={classes.paper} onClick={handleClose}>
                  <span>
                    <FcDocument size="25" />
                    <br />
                    <b>Documents</b>
                  </span>
                </td>
                <td className={classes.paper} onClick={handleClose}>
                  <span>
                    <FcCollaboration size="25" />
                    <br />
                    <b>Incidents</b>
                  </span>
                </td>
                <td className={classes.paper} onClick={handleClose}>
                  <span>
                    <FcReadingEbook size="25" />
                    <br />
                    <b>Alumni</b>
                  </span>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </Popover>
    </>
  );
}

export default Presentation;
