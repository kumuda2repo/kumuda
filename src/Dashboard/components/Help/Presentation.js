import React from "react";
import HelpIcon from "@material-ui/icons/Help";

function Presentation() {
  return (
    <span>
      <HelpIcon />
    </span>
  );
}
export default Presentation;
