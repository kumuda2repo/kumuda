import React, { Component } from "react";
import Presentation from "./Presentation";
import history from "../../../history";

class Container extends Component {
  handleClick = () => {
    history.push("/");
    window.location.reload();
  };
  render() {
    return (
      <div>
        <Presentation data={this.props} handleClick={this.handleClick} />
      </div>
    );
  }
}

export default Container;
