import React from "react";
import { AppBar, Toolbar, CssBaseline, IconButton } from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import { HiMenuAlt1 } from "react-icons/hi";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Apps from "../Apps";
import Help from "../Help";
import Notifications from "../Notifications";
import ProfileIcon from "../Profileicon";
import kumudalogo from "../../../assets/kumudalogo.png";

const useStyles = makeStyles((theme) => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },

  menuButton: {
    marginRight: 36,
  },
}));

function Presentation(props) {
  const { data, handleDrawerClose, handleDrawerOpen } = props.data;
  const classes = useStyles();

  return (
    <div>
      <CssBaseline />
      <AppBar position="fixed" className={clsx(classes.appBar)}>
        <Toolbar style={{ display: "flex", justifyContent: "space-between"}}>
          <span style={{ display: "flex"}} >
            {data.open ? (
              <IconButton
                color="inherit"
                aria-label="open drawer"
                onClick={handleDrawerClose}
                edge="start"
                className={clsx(classes.menuButton)}
              >
                <HiMenuAlt1 size="25" />
              </IconButton>
            ) : (
              <IconButton
                color="inherit"
                aria-label="open drawer"
                onClick={handleDrawerOpen}
                edge="start"
                className={clsx(classes.menuButton)}
              >
                <MenuIcon size="25" />
              </IconButton>
            )}
            <img
              className="c-pointer"
              src={kumudalogo}
              alt="Kumuda_logo"
              style={{ height: "48px" }}
              onClick={props.handleClick}
            />
            {/* <Typography
              variant="h5"
              style={{ margin: "auto", cursor: "pointer" }}
            >
              KUMUDA
            </Typography> */}
          </span>
          <span>
            <span style={{ cursor: "pointer" }}>
              <IconButton color="inherit" id="topnavbar_help">
                <Help />
              </IconButton>
            </span>
            <span style={{ cursor: "pointer" }}>
              <IconButton color="inherit" id="topnavbar_apps">
                <Apps />
              </IconButton>
            </span>
            <span style={{ cursor: "pointer" }}>
              <IconButton color="inherit" id="topnavbar_notifications">
                <Notifications />
              </IconButton>
            </span>
            <span style={{ cursor: "pointer", float: "right" }}>
              <IconButton color="inherit" id="topnavbar_profileicon">
                <ProfileIcon />
              </IconButton>
            </span>
          </span>
        </Toolbar>
      </AppBar>
    </div>
  );
}
export default Presentation;
