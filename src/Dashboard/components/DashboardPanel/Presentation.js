import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Router } from "react-router-dom";
import TopNavbar from "../TopNavbar";
import SideNavbar from "../SideNavbar";
import Routes from "../../../Routes";
import history from "../../../history";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    fontSize: "15px",
    overflowX: "overlay",
  },
}));

function Presentation(props) {
  const classes = useStyles();
  const {
    data,
    handleDrawerClose,
    handleDrawerOpen,
    currentLoadedService,
  } = props;
  return (
    <div className={classes.root}>
      <Router history={history}>
        <TopNavbar
          data={data}
          handleDrawerOpen={handleDrawerOpen}
          handleDrawerClose={handleDrawerClose}
        />
        {currentLoadedService === "" ? null : (
          <SideNavbar
            data={data}
            handleDrawerOpen={handleDrawerOpen}
            handleDrawerClose={handleDrawerClose}
          />
        )}
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <Routes />
        </main>
      </Router>
    </div>
  );
}
export default Presentation;
