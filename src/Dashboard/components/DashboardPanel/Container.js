import React, { Component } from "react";
import Presentation from "./Presentation";
import { connect } from "react-redux";

class Container extends Component {
  constructor() {
    super();
    this.state = {
      open: false,
    };
  }

  handleDrawerOpen = () => {
    this.setState({
      open: true,
    });
  };

  handleDrawerClose = () => {
    this.setState({
      open: false,
    });
  };

  render() {
    // console.log(this.props);
    return (
      <span>
        <Presentation
          data={this.state}
          handleDrawerOpen={this.handleDrawerOpen}
          handleDrawerClose={this.handleDrawerClose}
          {...this.props}
        />
      </span>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentLoadedService: state.dashboard.currentLoadedService,
  };
};

export default connect(mapStateToProps)(Container);
