import React from "react";
import NotificationsIcon from "@material-ui/icons/Notifications";

function Presentation() {
  return (
    <span>
      <NotificationsIcon />
    </span>
  );
}
export default Presentation;
