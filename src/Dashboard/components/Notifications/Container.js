import React, { Component } from "react";
import Presentation from "./Presentation";

export default class Container extends Component {
  render() {
    return (
      <span>
        <Presentation />
      </span>
    );
  }
}
