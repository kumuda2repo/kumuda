import ACTION from "../actions/actionTypes";

const initialState = {
  currentLoadedService: "",
};

const dashboardReducer = (state = initialState, action) => {
  // console.log(action);
  switch (action.type) {
    case ACTION.REQUEST_SERVICE:
      return {
        ...state,
        currentLoadedService: action.payload,
      };
    default:
      return state;
  }
};

export default dashboardReducer;
