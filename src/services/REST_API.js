import axios from "axios"


const make_API_call = (req_method, url, data = {}) => {
    switch(req_method){
        case 'get' : return axios.get(url).then(res => res.data)

        case 'post' : return axios.post(url, data).then(res => res.data)

        case 'put' : return axios.put(url, data).then(res => res.data)

        case 'delete' : return axios.delete(url).then(res => res.data)

        default: return null
    }
}

export default make_API_call