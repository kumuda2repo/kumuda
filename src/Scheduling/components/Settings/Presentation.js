import React from "react";
import PropTypes from "prop-types";
import SwipeableViews from "react-swipeable-views";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import {
  AppBar,
  Tabs,
  Tab,
  Typography,
  Box,
  Paper,
  Card,
  List,
  ListItem,
  ListItemText,
} from "@material-ui/core";
import MaterialTable from "material-table";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}
TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};
function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    "aria-controls": `full-width-tabpanel-${index}`,
  };
}
// app bar styles
const useStyles = makeStyles((theme) => ({
  demo1: {
    backgroundColor: " #fffeff",
    boxShadow: "none",
  },
  material_icons: {
    fontfamily: "Material Icons",
    fontweight: " normal",
    fontstyle: "normal",
    fontsize: " 24px",
    lineheight: " 1",
    letterspacing: "normal",
    texttransform: " none",
    display: "inline-block",
    whitespace: " nowrap",
    wordwrap: "normal",
    direction: "ltr",
  },
}));

function Presentation() {
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  //table for email templates
  const [table ] = React.useState({
    columns: [
      { title: "Name", field: "name" },
      { title: "Status", field: "key" },
      { title: "Subject", field: "subject" },
      { title: "SentFrom", field: "sent" },
      {
        title: "Lastupdated",
        field: "update",
        type: "numeric",
      },
    ],
    data: [
      {
        name: "Leave_request",
        key: "completed",
        subject: "Health issue",
        sent: "task_noreply@...",
        update: "may 3,2019",
      },
    ],
  });

  //table for settings
  const [state, setState] = React.useState({
    columns: [
      { title: "Category Name", field: "name" },
      { title: "key", field: "key" },
      { title: "value", field: "value", type: "numeric" },
      {
        title: "Category",
        field: "category",
      },
    ],
    data: [
      {
        name: "Finefee component",
        key: "fee component",
        value: 163,
        category: "",
      },
    ],
  });

  return (
    <div>
      <br />
      {/* titlebar */}
      <div style={{ marginLeft: "30px", marginRight: "30px" }}>
        <AppBar position="static" color="default" className={classes.demo1}>
          <Tabs
            value={value}
            onChange={handleChange}
            indicatorColor="primary"
            textColor="primary"
            aria-label="full width tabs example"
          >
            <Tab label="Masters" {...a11yProps(0)} />
            <Tab label="Email Templates" {...a11yProps(1)} />
            <Tab label="Settings" {...a11yProps(2)} />
            <Tab label="Workflow & rules" {...a11yProps(3)} />
            <Tab label="Permissions" {...a11yProps(4)} />
          </Tabs>
        </AppBar>
        <br />

        <SwipeableViews
          axis={theme.direction === "rtl" ? "x-reverse" : "x"}
          index={value}
          onChangeIndex={handleChangeIndex}
        >
          <Paper elevation={2}>
            <TabPanel value={value} index={0} dir={theme.direction}>
              <div style={{ display: "flex" }}>
                {/* first card */}
                <Card style={{ width: "20%", marginRight: "20px" }}>
                  <h4 style={{ color: "#8185c2" }}>Scheduling Templates</h4>
                  <List component="nav">
                    <ListItem button>
                      <ListItemText primary="Timetable Templates" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText primary="Period Validity" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText primary="Classrooms" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText primary="Classroom Type" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText primary="Holidays" />
                    </ListItem>
                  </List>
                </Card>
                {/* second card */}
                <Card style={{ width: "20%" }}>
                  <h4 style={{ color: "#8185c2" }}>
                    {" "}
                    Automated scheduled setting
                  </h4>
                  <List component="nav">
                    <ListItem button>
                      <ListItemText primary="Meeting patterns" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText primary="Meeting patternn forms" />
                    </ListItem>
                    <ListItem button>
                      <ListItemText primary="faculty preferences" />
                    </ListItem>
                  </List>
                </Card>
              </div>
            </TabPanel>
          </Paper>

          {/* second tab */}
          <Paper elevation={2}>
            <TabPanel value={value} index={1} dir={theme.direction}>
              <MaterialTable
                className={classes.material_icons}
                title="Editable Example"
                columns={table.columns}
                data={table.data}
                editable={{
                  onRowUpdate: (newData, oldData) =>
                    new Promise((resolve) => {
                      setTimeout(() => {
                        resolve();
                        if (oldData) {
                          setState((prevState) => {
                            const data = [...prevState.data];
                            data[data.indexOf(oldData)] = newData;
                            return { ...prevState, data };
                          });
                        }
                      }, 600);
                    }),
                  onRowDelete: (oldData) =>
                    new Promise((resolve) => {
                      setTimeout(() => {
                        resolve();
                        setState((prevState) => {
                          const data = [...prevState.data];
                          data.splice(data.indexOf(oldData), 1);
                          return { ...prevState, data };
                        });
                      }, 600);
                    }),
                }}
              />
            </TabPanel>
          </Paper>
          <Paper>
            <TabPanel value={value} index={2} dir={theme.direction}>
              <MaterialTable
                className={classes.material_icons}
                title="Editable Example"
                columns={state.columns}
                data={state.data}
                editable={{
                  onRowAdd: (newData) =>
                    new Promise((resolve) => {
                      setTimeout(() => {
                        resolve();
                        setState((prevState) => {
                          const data = [...prevState.data];
                          data.push(newData);
                          return { ...prevState, data };
                        });
                      }, 600);
                    }),
                  onRowUpdate: (newData, oldData) =>
                    new Promise((resolve) => {
                      setTimeout(() => {
                        resolve();
                        if (oldData) {
                          setState((prevState) => {
                            const data = [...prevState.data];
                            data[data.indexOf(oldData)] = newData;
                            return { ...prevState, data };
                          });
                        }
                      }, 600);
                    }),
                  onRowDelete: (oldData) =>
                    new Promise((resolve) => {
                      setTimeout(() => {
                        resolve();
                        setState((prevState) => {
                          const data = [...prevState.data];
                          data.splice(data.indexOf(oldData), 1);
                          return { ...prevState, data };
                        });
                      }, 600);
                    }),
                }}
              />
            </TabPanel>
          </Paper>
        </SwipeableViews>
      </div>
    </div>
  );
}

export default Presentation;
