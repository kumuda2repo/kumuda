import React, { useEffect } from "react";
import { Router, Switch, Route} from "react-router-dom";
import DashboardPanel from "./Dashboard/components/DashboardPanel";
import { connect } from "react-redux";
import Login from "./Authentication/components/Login";
import { _on_page_refresh } from "./Authentication/middleware/middleware";
import { _set_value_in_store } from "./Dashboard/middleware/middleware";
import history from "./history";

function App(props) {
  // console.log(history.location.pathname);
  useEffect(() => {
    if (!props.auth && props.loginstatus === "ideal") {
      props.onPageRefresh();
      let path = history.location.pathname.split("/");
      // console.log(path);
      props.setvalueinstore(path[1]);
    }
  });

  return (
    <div className="App">
      {props.auth ? (
        <DashboardPanel />
      ) : (
        <Router history={history}>
          <Switch>
            <Route exact path="/" component={Login} />
          </Switch>
        </Router>
      )}
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth.isAuthenticated,
    loginstatus: state.auth.loginStatus,
    currentLoadedService: state.dashboard.currentLoadedService,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onPageRefresh: () => dispatch(_on_page_refresh()),
    setvalueinstore: (serviceName) =>
      dispatch(_set_value_in_store(serviceName)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
