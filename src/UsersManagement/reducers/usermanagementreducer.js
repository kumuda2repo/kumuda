import ACTION from "../actions/actionTypes";

const initialState = {
  premissionsStatus: "ideal",
  premissionUsers: [],
  workflowandRulesStatus: "",
  workflowandRulesUsers: [],
  settingsStatus: "ideal",
  settingsUsers: [],
  RoleManagementStatus: "ideal",
  RoleManagementRoles: [],
  bulkusersStatus: "",
  singleuserStatus: "",
  historyStatus: "",
  history: [],
  studentlistStatus: "",
  studentlistUsers: [],
  employeelistStatus: "ideal",
  employeelistUsers: [],
  studenttemplatesStatus: "",
  studentFields: [],
  facultytemplatesStatus: "",
  facultyFields: [],
  isupdatingtemplate: false,
  emailtemplatesStatus: "ideal",
  letterTypes: [],
  defaultContent: `<div className='Date'>
        <span id='variable_todaydate'>~TodayDate~</span>
      </div>
      
      <center>
        <h1> Offer Letter </h1>
      </center>
      <div className='Address'>
        <span id='variable_universityname'>~UniversityName~</span>
        <br />
        <span id='variable_universityaddress'>~UniversityAddress~</span>
      </div>
      <p>
        This is to verify that Mr.<span id='variable_firstname'>~FirstName~</span><span id='variable_firstname'></span> 
        <span id='variable_lastname'>~LastName~</span> has been offered a full time position
        (40hr/Week), with <span id='variable_companyname'>~CompanyName~</span> as <span id='variable_jobtitle'>~JobTitle~</span> 
        effective from <span id='variable_effectivedate'>~EffectiveDate~</span>. This offer will be a
        knowledge based internship during the above mentioned period. Please
        note that this offer is contingent on successful CPT approval As a 
        <span id='variable_jobtitle'>~JobTitle~</span> Mr. <span id='variable_firstname'>~FirstName~</span> 
        <span id='variable_lastname'>~LastName~</span> will perform the following job duties:
      </p>
      
      <p>
        I certify that our organization <span id='variable_companyname'>~CompanyName~</span> is registered
        E-verify Company. The E-Verify Company Identification Number is 
        <span id='variable_everifynumber'>~CompanyEverifyNumber~</span>. Our Companies EIN is 
        <span  id='variable_einnumber'>~CompanyEIN~</span>. I agree to comply with the reporting
        requirements for students on authorized Curricular Practical Training
        (CPT). I agree to report the termination or departure of the student to
        you and to the university within 48 hours of termination of employment.
        You will be in our branch office at <span  id='variable_companyname'>~CompanyName~</span>, 
        <span  id='variable_companyaddress'>~CompanyAddress~</span>.
      </p>
      
      <p>
        If any additional information is required, please contact me at 
        <span id='variable_hrmailid'>~HRMailId~</span>
      </p>
      <div className='final'>
        <p>
          Sincerely,
          <br />
          <span id='variable_authorizedSign'>~AuthorizedPersonSignature~</span>
          <br />
          <span id='variable_authorizedname'>~AuthorizedName~</span>
          <br />
          <span id='variable_designation'>~AuthorizedPersonDesignation~</span>
        </p>
      </div>`,
  isDeleting: false,
};

const usermanagementreducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION.SET_STATE:
      return {
        ...state,
        isupdatingtemplate: true,
      };

    case ACTION.PREMISSIONS_REQUEST:
      return {
        ...state,
        premissionsStatus: "requesting",
      };

    case ACTION.PREMISSIONS_SUCCESS:
      console.log(action);
      return {
        ...state,
        premissionsStatus: "success",
        premissionUsers: action.payload,
      };

    case ACTION.PREMISSIONS_FAILURE:
      return {
        ...state,
        premissionsStatus: "failure",
      };

    case ACTION.WORKFLOWANDRULES_REQUEST:
      return {
        workflowandRulesStatus: "requesting",
      };

    case ACTION.WORKFLOWANDRULES_SUCCESS:
      return {
        ...state,
        workflowandRulesStatus: "success",
        workflowandRulesUsers: action.payload,
      };

    case ACTION.WORKFLOWANDRULES_FAILURE:
      return {
        ...state,
        workflowandRulesStatus: "failure",
        workflowandRulesUsers: action.payload,
      };

      case ACTION.SETTINGS_REQUEST:
        return {
          ...state,
          settingsStatus: "requesting",
        };
      case ACTION.SETTINGS_SUCCESS:
        return {
          ...state,
          settingsStatus: "Success",
          settingsUsers: action.payload,
        };
      case ACTION.SETTINGS_FAILURE:
        return {
          ...state,
          settingsStatus: "failure",
        };
        ///////////////////////////
        case ACTION.ROLEMANAGEMENT_REQUEST:
          return {
            ...state,
            RoleManagementStatus: "requesting",
          };
        case ACTION.ROLEMANAGEMENT_SUCCESS:
          return {
            ...state,
            RoleManagementStatus: "Success",
            RoleManagementRoles: action.payload,
          };
        case ACTION.ROLEMANAGEMENT_FAILURE:
          return {
            ...state,
            RoleManagementStatus: "failure",
          };

    case ACTION.BULKUSERS_REQUEST:
      return {
        ...state,
        bulkusersStatus: "requesting",
      };

    case ACTION.BULKUSERS_SUCCESS:
      return {
        ...state,
        bulkusersStatus: "success",
      };

    case ACTION.BULKUSERS_FAILURE:
      return {
        ...state,
        bulkusersStatus: "failure",
      };

    case ACTION.SINGLEUSER_REQUEST:
      return {
        ...state,
        singleuserStatus: "requesting",
      };
    case ACTION.SINGLEUSER_SUCCESS:
      return {
        ...state,
        singleuserStatus: "success",
      };
    case ACTION.SINGLEUSER_FAILURE:
      return {
        ...state,
        singleuserStatus: "failure",
      };

    case ACTION.HISTORY_REQUEST:
      return {
        ...state,
        historyStatus: "requesting",
      };
    case ACTION.HISTORY_SUCCESS:
      return {
        ...state,
        historyStatus: "success",
        history: action.payload,
      };
    case ACTION.HISTORY_FAILURE:
      return {
        ...state,
        historyStatus: "failure",
      };

    case ACTION.STUDENTTEMPLATES_REQUEST:
      return {
        ...state,
        studenttemplatesStatus: "requesting",
      };
    case ACTION.STUDENTTEMPLATES_SUCCESS:
      return {
        ...state,
        studenttemplatesStatus: "success",
        studentFields: action.payload,
      };
    case ACTION.STUDENTTEMPLATES_FAILURE:
      return {
        ...state,
        studenttemplatesStatus: "failure",
      };

    case ACTION.STUDENT_REQUEST:
      return {
        ...state,
        studentlistStatus: "requesting",
      };

    case ACTION.STUDENT_REQUEST_SUCCESS:
      return {
        ...state,
        studentlistStatus: "success",
        studentlistUsers: action.payload,
      };
    case ACTION.STUDENT_REQUEST_FAILURE:
      return {
        ...state,
        studentlistStatus: "failure",
        studentlistUsers: action.payload,
      };
    //employee reducer
    case ACTION.EMPLOYEE_REQUEST:
      return {
        ...state,
        employeelistStatus: "requesting",
      };

    case ACTION.EMPLOYEE_REQUEST_SUCCESS:
      return {
        ...state,
        employeelistStatus: "success",
        employeelistUsers: action.payload,
      };

    case ACTION.EMPLOYEE_REQUEST_FAILURE:
      return {
        ...state,
        employeelistStatus: "failure",
      };

    case ACTION.FACULTYTEMPLATES_REQUEST:
      return {
        ...state,
        facultytemplatesStatus: "requesting",
      };
    case ACTION.FACULTYTEMPLATES_SUCCESS:
      return {
        ...state,
        facultytemplatesStatus: "success",
        facultyFields: action.payload,
      };
    case ACTION.FACULTYTEMPLATES_FAILURE:
      return {
        ...state,
        facultytemplatesStatus: "failure",
      };

    case ACTION.EMAILTEMPLATES_REQUEST:
      return {
        ...state,
        emailtemplatesStatus: "requesting",
      };
    case ACTION.EMAILTEMPLATES_SUCCESS:
      return {
        ...state,
        emailtemplatesStatus: "success",
        letterTypes: action.payload,
      };
    case ACTION.EMAILTEMPLATES_FAILURE:
      return {
        ...state,
        emailtemplatesStatus: "failure",
      };

    case ACTION.STUDENTDELETE_REQUEST:
      return {
        ...state,
        isDeleting: true
      }

      case ACTION.STUDENTDELETE_SUCCESS:
        return {
          ...state,
          isDeleting: false
        }  
  

    case ACTION.STUDENTDELETE_FAILURE:
      return {
        ...state,
        isDeleting: false
      }  

    default:
      return state;
  }
};

export default usermanagementreducer;
