import axios from "axios";
import {
  StudentDeleteFailure,
  StudentDeleteRequest,
  StudentDeleteSuccess,
  setState,
  premissionRequest,
  premissionRequesSuccess,
  workflowandrulesRequest,
  workflowandrulesSuccesss,
  bulkusersRequest,
  bulkusersSuccess,
  bulkusersFailure,
  employeeRequest,
  employeeRequestSuccess,
  historyRequest,
  historySuccess,
  studentTemplateRequest,
  studentTemplateSuccess,
  facultyTemplateRequest,
  facultyTemplateSuccess,
  emailTemplatesSuccess,
  emailTemplatesRequest,
  singleUserRequest,
  singleUserSuccess,
  studentRequest,
  studentRequestSuccess,
  settingsRequest,
  settingSuccess,
  RoleManagementSuccess,
  RoleManagementRequest,
} from "../actions/actionCreators";
import {
  waitingMsg,
  stopWaitMsg,
  errorMsg,
  successMsg,
} from "../../SharedComponents/AllSanckBars";
import {
  student_template,
  employee_template,
} from "../../SharedComponents/TemplateFields";

const base_url = window.location.href.includes("http://localhost:3005")
  ? "http://localhost:5000/kumuda-test/us-central1/api"
  : "https://us-central1-kumuda-test.cloudfunctions.net/api";

export const _premissions = () => (dispatch) => {
  // waitingMsg("Please wait...")
  dispatch(premissionRequest());
  const dummyData = [
    {
      name: "Praveen",
      mail: "praveen@gmail.com",
      role: "student",
      create: true,
      update: false,
      read: false,
      delete: false,
    },
    {
      name: "Murali",
      mail: "murali@gmail.com",
      role: "student",
      create: false,
      update: false,
      read: false,
      delete: false,
    },
    {
      name: "Sandeep",
      mail: "Sandeep@gmail.com",
      role: "student",
      create: false,
      update: false,
      read: false,
      delete: false,
    },
    {
      name: "Sumasri",
      mail: "Sumasri@gmail.com",
      role: "student",
      create: false,
      update: false,
      read: false,
      delete: false,
    },
    {
      name: "Eshwara",
      mail: "Eshwara@gmail.com",
      role: "student",
      create: false,
      update: false,
      read: false,
      delete: false,
    },
    {
      name: "Kalyan",
      mail: "Kalyan@gmail.com",
      role: "student",
      create: false,
      update: false,
      read: false,
      delete: false,
    },
    {
      name: "Dr.R.B.C",
      mail: "dean@gmail.com",
      role: "dean",
      create: false,
      update: false,
      read: false,
      delete: false,
    },
    {
      name: "Dr.Ravi",
      maeil: "facutly@gmail.com",
      role: "facutly",
      create: false,
      update: false,
      read: false,
      delete: false,
    },
    {
      name: "ECEHOD",
      mail: "hod@gmail.com",
      role: "hod",
      create: false,
      update: false,
      read: false,
      delete: false,
    },
  ];
  // stopWaitMsg()
  dispatch(premissionRequesSuccess(dummyData));
  // successMsg("success message")
};

export const _workflowandRules = () => (dispatch) => {
  dispatch(workflowandrulesRequest());
  const sampleData = [
    {
      name: "Praveen",
      mail: "praveen@gmail.com",
      role: "student",
      entryBy: true,
      authorise: false,
      approve: false,
    },
    {
      name: "Murali",
      mail: "murali@gmail.com",
      role: "student",
      entryBy: false,
      authorise: false,
      approve: false,
    },
    {
      name: "Sandeep",
      mail: "Sandeep@gmail.com",
      role: "student",
      entryBy: false,
      authorise: false,
      approve: false,
    },
    {
      name: "Sumasri",
      mail: "Sumasri@gmail.com",
      role: "student",
      entryBy: false,
      authorise: false,
      approve: false,
    },
    {
      name: "Eshwara",
      mail: "Eshwara@gmail.com",
      role: "student",
      entryBy: false,
      authorise: false,
      approve: false,
    },
    {
      name: "Kalyan",
      mail: "Kalyan@gmail.com",
      role: "student",
      entryBy: false,
      authorise: false,
      approve: false,
    },
    {
      name: "Dr.R.B.C",
      mail: "dean@gmail.com",
      role: "dean",
      entryBy: false,
      authorise: false,
      approve: false,
    },
    {
      name: "Dr.Ravi",
      mail: "facutly@gmail.com",
      role: "facutly",
      entryBy: false,
      authorise: false,
      approve: false,
    },
    {
      name: "ECEHOD",
      mail: "hod@gmail.com",
      role: "hod",
      entryBy: false,
      authorise: false,
      approve: false,
    },
  ];
  dispatch(workflowandrulesSuccesss(sampleData));
};

export const _settings = () => (dispatch) => {
  dispatch(settingsRequest());
  const sampleData = [
    {
      name: "DR P Kiran Kumar",
      CSE: false,
      ECE: false,
      IT: false,
      CE: false,
      ME: false,
      MBA: false,
    },
    {
      name: "Dr.D Prasad",
      CSE: false,
      ECE: false,
      IT: false,
      CE: false,
      ME: false,
      MBA: false,
    },
    {
      name: "Dr.T J V Subrahmanyeswara Rao",
      CSE: false,
      ECE: false,
      IT: false,
      CE: false,
      ME: false,
      MBA: false,
    },
    {
      name: "C R S Hanuman",
      CSE: false,
      ECE: false,
      IT: false,
      CE: false,
      ME: false,
      MBA: false,
    },
    {
      name: "Srinivas",
      CSE: false,
      ECE: false,
      IT: false,
      CE: false,
      ME: false,
      MBA: false,
    },
  ];
  dispatch(settingSuccess(sampleData));
};
///////role management
export const _RoleManagement = () => (dispatch) => {
  dispatch(RoleManagementRequest());
  const sampleData = [
    {
      name: "DR P kiran Kumar",
      Faculty: false,
      Student: false,
      Dean: false,
      HOD: false,
    },
    {
      name: "Dr.D Prasad",
      Faculty: false,
      Student: false,
      Dean: false,
      HOD: false,
    },
    {
      name: "Dr.T J V Subrahmanyeswara Rao",
      Faculty: false,
      Student: false,
      Dean: false,
      HOD: false,
    },
    {
      name: "C R S Hanuman",
      Faculty: false,
      Student: false,
      Dean: false,
      HOD: false,
    },
    {
      name: "Srinivas",
      Faculty: false,
      Student: false,
      Dean: false,
      HOD: false,
    },
  ];
  dispatch(RoleManagementSuccess(sampleData));
};

//EmployeeData
export const _employeeList = (role) => (dispatch) => {
  waitingMsg("Please wait...");
  dispatch(employeeRequest());
  console.log(role);
  return axios
    .post(`${base_url}/default/readAll`, {
      role: role,
      path: "/UserManagement/employeelist/ECE",
    })
    .then((res) => {
      console.log(res.data);
      const employeeData = res.data;
      stopWaitMsg();
      dispatch(employeeRequestSuccess(employeeData));
    })
    .catch((err) => {
      console.log(err);
      stopWaitMsg();
      errorMsg("Unable to Fetch the Employees Data");
    });
};

export const _studentList = (role) => (dispatch) => {
  // waitingMsg("Please wait ...");
  dispatch(studentRequest());
  console.log(role);
  return axios
    .post(`${base_url}/default/readAll`, {
      role: role,
      path: "/UserManagement/studentlist/CSE",
    })
    .then((res) => {
      const studentdata = res.data;
      stopWaitMsg();
      dispatch(studentRequestSuccess(studentdata));
    })
    .catch((err) => {
      console.log(err);
      stopWaitMsg();
      errorMsg("Unable to Fetch the Student Data");
    });
};

export const _history = () => (dispatch) => {
  // waitingMsg("Please wait...");
  dispatch(historyRequest());
  return axios
    .get(`${base_url}/history/readAll/2020`)
    .then((res) => {
      const sampleData = res.data;
      // stopWaitMsg();
      dispatch(historySuccess(sampleData));
    })
    .catch((err) => {
      console.log(err);
      // stopWaitMsg();
      errorMsg("Unable to Fetch History");
    });
};
export const _studentTemplates = (id) => (dispatch) => {
  waitingMsg("Please wait...");
  dispatch(studentTemplateRequest());
  return axios
    .post(`${base_url}/default/read`, {
      id: "StudentTemplate",
      path: "/UserManagement/settings/Templates",
    })
    .then((res) => {
      console.log(res.data);
      const student_template = res.data;
      stopWaitMsg();
      dispatch(studentTemplateSuccess(student_template.sections));
    })
    .catch(error=>console.log(error))
};

export const _facultyTemplates = (id) => (dispatch) => {
  waitingMsg("Please wait...");
  dispatch(facultyTemplateRequest());
  return axios
    .post(`${base_url}/default/read`, {
      id: id,
      path: "/UserManagement/settings/Templates",
    })
    .then((res) => {
      console.log(res.data);
      const employee_template = res.data;
      stopWaitMsg();
      dispatch(facultyTemplateSuccess(employee_template));
    });
};

export const _bulkusers = (user, auth_create, history_payload) => (
  dispatch
) => {
  // console.log(user, auth_create);
  waitingMsg("Please wait...");
  dispatch(bulkusersRequest());
  auth_create.forEach((authUser, index) => {
    console.log(authUser, user[index], history_payload[index]);
    return axios
      .post(`${base_url}/default/create`, authUser, {
        path: "/UserManagement/employeelist/Department",
      })
      .then((res) => {
        console.log(res.data);
        axios
          .post(`${base_url}/user/create`, user[index])
          .then((res) => {
            console.log(res.data);
            stopWaitMsg();
            successMsg("All Users are Created");
            dispatch(bulkusersSuccess());
            axios
              .post(`${base_url}/history/create`, history_payload[index])
              .then((res) => console.log(res.data))
              .catch((err) => console.error(err));
          })
          .catch((err) => {
            console.log(err);
            stopWaitMsg();
            dispatch(bulkusersFailure());
            errorMsg("Unable to Add User");
          });
      })
      .catch((err) => {
        console.log(err);
        stopWaitMsg();
        dispatch(bulkusersFailure());
        errorMsg("Unable to Create Users");
      });
  });
};
export const _singleUser = (user, auth_create, history_payload) => (
  dispatch
) => {
  waitingMsg("Please wait...");
  dispatch(singleUserRequest());
  console.log(auth_create, user);
  return axios
    .post(`${base_url}/auth/register`, auth_create)
    .then((res) => {
      console.log(res);
      axios
        .post(`${base_url}/user/create`, user)
        .then((res) => {
          console.log(res.data);
          stopWaitMsg();
          successMsg("User is Created");
          dispatch(singleUserSuccess());
          axios
            .post(`${base_url}/history/create`, history_payload)
            .then((res) => console.log(res.data))
            .catch((err) => console.error(err));
        })
        .catch((err) => {
          console.log(err);
          stopWaitMsg();
          errorMsg("Unable to create User");
        });
    })
    .catch((err) => {
      console.log(err);
      stopWaitMsg();
      errorMsg("Unable to Add User");
    });
};

export const _emailTemplates = () => (dispatch) => {
  dispatch(emailTemplatesRequest());
  const sampleData = [
    { name: "Forgot Password" },
    { name: "User Invitation" },
    { name: "Updates" },
  ];
  stopWaitMsg();
  dispatch(emailTemplatesSuccess(sampleData));
};

export const _setState = () => (dispatch) => {
  dispatch(setState());
};
export const _deletestudent = (id) => (dispatch) => {
  alert(id);
  dispatch(StudentDeleteRequest());
  return axios
    .delete(`${base_url}/default/delete`, {
      id: id,
      path: "/UserManagement/studentlist/CSE",
    })
    .then((res) => {
      // const studentdata=res.data;
      stopWaitMsg();
      dispatch(StudentDeleteSuccess());
      successMsg("successfully deleted user");
    })
    .catch((err) => {
      console.log(err.response);
      stopWaitMsg();

      errorMsg("Unable to delete the Student Data");
      dispatch(StudentDeleteFailure());
    });
};
