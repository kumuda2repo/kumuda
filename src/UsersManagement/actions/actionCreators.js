import ACTION from "./actionTypes";

//PERMISSIONS action creators
export const premissionRequest = () => {
  return {
    type: ACTION.PREMISSIONS_REQUEST,
  };
};
export const premissionRequesSuccess = (dummyData) => {
  return {
    type: ACTION.PREMISSIONS_SUCCESS,
    payload: dummyData,
  };
};
export const premissionRequesFailure = () => {
  return {
    type: ACTION.PREMISSIONS_FAILURE,
  };
};

//WORKFLOWANDRULES actioncreators
export const workflowandrulesRequest = () => {
  return {
    type: ACTION.WORKFLOWANDRULES_REQUEST,
  };
};
export const workflowandrulesSuccesss = (sampleData) => {
  return {
    type: ACTION.WORKFLOWANDRULES_SUCCESS,
    payload: sampleData,
  };
};
export const workflowandrulesFailure = () => {
  return {
    type: ACTION.WORKFLOWANDRULES_FAILURE,
  };
};

//settings Actions Creators
export const settingsRequest = () => {
  return {
    type: ACTION.SETTINGS_REQUEST,
  };
};
export const settingSuccess = (sampleData) => {
  return {
    type: ACTION.SETTINGS_SUCCESS,
    payload: sampleData,
  };
};
export const settingsFailure = () => {
  return {
    type: ACTION.SETTINGS_FAILURE,
  };
};

//RoleManagement Actions Creators
export const RoleManagementRequest = () => {
  return {
    type: ACTION.ROLEMANAGEMENT_REQUEST,
  };
};
export const RoleManagementSuccess = (sampleData) => {
  return {
    type: ACTION.ROLEMANAGEMENT_SUCCESS,
    payload: sampleData,
  };
};
export const RoleManagementFailure = () => {
  return {
    type: ACTION.ROLEMANAGEMENT_FAILURE,
  };
};

//BULK USERS action creators
export const bulkusersRequest = () => {
  return {
    type: ACTION.BULKUSERS_REQUEST,
  };
};
export const bulkusersSuccess = () => {
  return {
    type: ACTION.BULKUSERS_SUCCESS,
  };
};
export const bulkusersFailure = () => {
  return {
    type: ACTION.BULKUSERS_FAILURE,
  };
};

//History action creators
export const historyRequest = () => {
  return {
    type: ACTION.HISTORY_REQUEST,
  };
};
export const historySuccess = (sampleData) => {
  return {
    type: ACTION.HISTORY_SUCCESS,
    payload: sampleData,
  };
};
export const historyFailure = () => {
  return {
    type: ACTION.HISTORY_FAILURE,
  };
};

//STUDENT TEMPLETE action creators
export const studentTemplateRequest = () => {
  return {
    type: ACTION.STUDENTTEMPLATES_REQUEST,
  };
};
export const studentTemplateSuccess = (StudentTemplate) => {
  return {
    type: ACTION.STUDENTTEMPLATES_SUCCESS,
    payload: StudentTemplate,
  };
};
export const studentTemplateFailure = () => {
  return {
    type: ACTION.STUDENTTEMPLATES_FAILURE,
  };
};

// Student List Action Creators
export const studentRequest = () => {
  return {
    type: ACTION.STUDENT_REQUEST,
  };
};
export const studentRequestSuccess = (studentdata) => {
  return {
    type: ACTION.STUDENT_REQUEST_SUCCESS,
    payload: studentdata,
  };
};
export const studentRequestFailure = (error) => {
  return {
    type: ACTION.STUDENT_REQUEST_FAILURE,
    payload: error,
  };
};

//Employee List Action Creators
export const employeeRequest = () => {
  return {
    type: ACTION.EMPLOYEE_REQUEST,
  };
};
export const employeeRequestSuccess = (employeeData) => {
  return {
    type: ACTION.EMPLOYEE_REQUEST_SUCCESS,
    payload: employeeData,
  };
};
export const employeeRequestFailure = (error) => {
  return {
    type: ACTION.EMPLOYEE_REQUEST_FAILURE,
    payload: error,
  };
};

//FACULTY TEMPLETE action creators
export const facultyTemplateRequest = () => {
  return {
    type: ACTION.FACULTYTEMPLATES_REQUEST,
  };
};
export const facultyTemplateSuccess = (FacultyTemplate) => {
  return {
    type: ACTION.FACULTYTEMPLATES_SUCCESS,
    payload: FacultyTemplate,
  };
};
export const facultyTemplateFailure = () => {
  return {
    type: ACTION.FACULTYTEMPLATES_FAILURE,
  };
};

//EMAIL TEMPLATE action creators
export const emailTemplatesRequest = () => {
  return {
    type: ACTION.EMAILTEMPLATES_REQUEST,
  };
};
export const emailTemplatesSuccess = (sampleData) => {
  return {
    type: ACTION.EMAILTEMPLATES_SUCCESS,
    payload: sampleData,
  };
};
export const emailTemplatesFailure = () => {
  return {
    type: ACTION.EMAILTEMPLATES_FAILURE,
  };
};

//SINGLE USER action creators
export const singleUserRequest = () => {
  return {
    type: ACTION.SINGLEUSER_REQUEST,
  };
};
export const singleUserSuccess = () => {
  return {
    type: ACTION.SINGLEUSER_SUCCESS,
  };
};
export const singleUserFailure = () => {
  return {
    type: ACTION.SINGLEUSER_FAILURE,
  };
};

//REPORTS action creators
export const reportsRequest=()=>{
  return{
    type:ACTION.REPORTS_REQUEST,
  }
}
export const reportsSuccess=()=>{
  return{
    type: ACTION.REPORTS_SUCCESS,
  }
}
export const reportsFailure=()=>{
  return{
    type:ACTION.REPORTS_FAILURE,
  }
}

export const setState = () => {
  return {
    type: ACTION.SET_STATE,
  };
};

// student delete action creators
export const StudentDeleteFailure = () => {
  return {
    type: ACTION.STUDENTDELETE_FAILURE,
    
  };
};
export const StudentDeleteSuccess = () => {
  return {
    type: ACTION.STUDENTDELETE_SUCCESS,
    
  };
};
export const StudentDeleteRequest = () => {
  return {
    type: ACTION.STUDENTDELETE_REQUEST,
  
  };
};