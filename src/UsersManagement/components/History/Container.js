import React, { Component } from "react";
import Presentation from "./Presentation";
import { _history } from "../../middleware/middleware";
import { connect } from "react-redux";

class Container extends Component {
  constructor(props) {
    super(props);
    console.log(props.history);
    this.state = {
      history: props.history,
    };
  }
  componentDidMount() {
    this.props.historyData();
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      history: nextProps.history,
    });
  }
  render() {
    return (
      <div>
        <Presentation {...this.state} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    history: state.usermanagement.history,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    historyData: () => {
      dispatch(_history());
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Container);
