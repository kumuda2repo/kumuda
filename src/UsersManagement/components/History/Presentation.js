import React from "react";
import Paper from "@material-ui/core/Paper";
//import MaterialTable from "material-table";
import CustomTable from '../../../SharedComponents/MaterialTable'
export default function Presentation(Props) {
  const { history=[] } = Props;
  const userHistroy = [];
  history.length &&
    history.forEach((user_histroy) => {
      if (Object.keys(user_histroy).length) {
        const time = new Date(user_histroy.date);
        const time_Stamp =
          time.toLocaleDateString() + " " + time.toLocaleTimeString();
        userHistroy.push({
          actionby: user_histroy.actionBy,
          timestamp: time_Stamp,
          history: user_histroy.action,
        });
      }
    });
  return (
    <div>
      <Paper>
        < CustomTable
          title={<h2>History</h2>}
          columns={[
            { title: "Action by", field: "actionby" },
            { title: "Time Stamp", field: "timestamp" },
            { title: "History", field: "history" },
          ]}
          data={userHistroy}
          options={{
            search: true,
          }}
        />
      </Paper>
    </div>
  );
}
