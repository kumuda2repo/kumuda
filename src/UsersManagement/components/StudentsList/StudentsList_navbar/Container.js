import React,{Component} from "react";
import Presentation from "./Presentation";
import { connect } from "react-redux";
import { _studentList } from "../../../middleware/middleware"

 class Container extends Component {
  constructor(props) {
    super(props)
    this.state = {
      studentlist: props.studentlistUsers,
      role: "Students"
    }
  }

  componentDidMount() {
    console.log(this.props)
    this.props.studentdetails(this.state.role)
  }

  componentWillReceiveProps(nextProps) {
    console.log(nextProps)
    this.setState({
      studentlist: nextProps.studentlistUsers
    })
  }

  render() {
  
    return (
      <div>
        <Presentation {...this.state} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
 // console.log(state.usermanagement)
  return {
    studentlistUsers: state.usermanagement.studentlistUsers
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    studentdetails: (role) => {
      console.log(role)
      dispatch(_studentList(role))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Container);
