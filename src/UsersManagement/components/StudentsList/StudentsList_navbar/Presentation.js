import React from "react";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Badge from "@material-ui/core/Badge";
import Studentlist from "../StudentsList_Table";
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-force-tabpanel-${index}`}
      aria-labelledby={`scrollable-force-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-force-tab-${index}`,
    "aria-controls": `scrollable-force-tabpanel-${index}`,
  };
}

// const useStyles = makeStyles((theme) => ({
//   root: {
//     backgroundColor: theme.palette.background.paper,
//     width: 500,
//   },
//   demo1: {
//     backgroundColor: " #fffeff",
//     boxShadow: "none",
//   },
// }));

export default function Presentation(props) {
  const { studentlist=[] } = props;
//  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  console.log(studentlist);
  return (
  
    <div style={{ backgroundColor: "#fff" }}>
     
      <AppBar position="static" color="default" >
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
          aria-label="scrollable auto tabs example"
        >
          <Tab
            label={
              <Badge
                color="primary"
                children="All"
                max="10000"
                badgeContent={studentlist.length}
              />
            }
            {...a11yProps(0)}
          />
          <Tab
            label={
              <Badge
                color="primary"
                children="Active"
                max="10000"
                badgeContent={
                  studentlist.filter(
                    (studentlist) => studentlist.status === "Active"
                  ).length
                }
              />
            }
            {...a11yProps(1)}
          />
          <Tab
            label={
              <Badge
                color="primary"
                children="Inactive"
                max="10000"
                badgeContent={
                  studentlist.filter(
                    (studentlist) => studentlist.status === "Inactive"
                  ).length
                }
              />
            }
            {...a11yProps(2)}
          />
          <Tab
            label={
              <Badge
                color="error"
                children="Suspended"
                max="10000"
                badgeContent={
                  studentlist.filter(
                    (studentlist) => studentlist.status === "Suspended"
                  ).length
                }
              />
            }
            {...a11yProps(3)}
          />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <Studentlist all={true} studentlist={studentlist} />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Studentlist
          active={true}
          studentlist={studentlist.filter(
            (studentlist) => studentlist.status === "Active"
          )}
        />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <Studentlist
          inactive={true}
          studentlist={studentlist.filter(
            (studentlist) => studentlist.status === "Inactive"
          )}
        />
      </TabPanel>
      <TabPanel value={value} index={3}>
        <Studentlist
          suspend={true}
          studentlist={studentlist.filter(
            (studentlist) => studentlist.status === "Suspended"
          )}
        />
      </TabPanel>
      <TabPanel value={value} index={4}></TabPanel>
    </div>
  );
}
