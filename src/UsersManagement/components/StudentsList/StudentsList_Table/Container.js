import Alert from "@material-ui/lab/Alert";
import React, { Component } from "react";
import Presentation from "./Presentation";
import {_deletestudent} from "../../../middleware/middleware"
import { connect } from "react-redux";

class Container extends Component {
  constructor(props) {
    super(props);
    console.log(props, "9");
  //  this.delete= this.delete.bind(this);
    
     this.state = {
      studentlist: props.studentlist,
      del:false
     }
  }
   //handleDelete =() =>{
     //consol
     //this.state.del = false
 // }
   handleAgree = (id) => {
     this.props.Deletestudent(id)
    //  alert("delete user");
    
   }
   

  render() {
    console.log(this.props.studentlist);
    return (
      <div>
        <Presentation studentlist={this.props.studentlist}
        handleAgree={this.handleAgree}


         />

      </div>
    );
  }
}

const mapStateToProps = (state) => {
  // console.log(state.usermanagement)
   return {
     studentlistUsers: state.usermanagement.studentlistUsers
   }
 }
 
 const mapDispatchToProps = (dispatch) => {
   return {
    Deletestudent: (id) => dispatch(_deletestudent(id))
   }
 }
 
 export default connect(mapStateToProps, mapDispatchToProps)(Container);
