import React from "react";
import { Link } from "react-router-dom";
import AddCircleOutlineRoundedIcon from "@material-ui/icons/AddCircleOutlineRounded";

import { Button } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
//import { CustomField } from "../../../../SharedComponents/FormFeilds";
//import Grid from "@material-ui/core/Grid";
//import InviteThroughExcel from "../../Settings/Masters/InviteThroughExcel";
import CreateTwoToneIcon from "@material-ui/icons/CreateTwoTone";
import VisibilityTwoToneIcon from "@material-ui/icons/VisibilityTwoTone";
import DeleteOutlineTwoToneIcon from "@material-ui/icons/DeleteOutlineTwoTone";
import CustomTable from "../../../../SharedComponents/MaterialTable";
import TableActions from "../../../../SharedComponents/TableActions";
import BulkUser_Creation from "../../../../Pages/BulkUser_Creation";

export default function Presentation(props) {
  const { studentlist,handleAgree } = props;
  //const { data, handleChange, handleClickShowPassword, handleCreate } = props;

  //const { value } = props;

  const allStudents = [];
  studentlist.length &&
    studentlist.forEach((student) => {
      console.log(student);
      if (Object.keys(student).length) {
        allStudents.push({
          name: student.name,
          gender: student.gender,
          email: student.email,
          phone: student.phone,
          branch: student.department,
          department: student.department,
          studentid: student.id,
          through: student.through,
          status: student.status,
        });
      }
    });
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const [edit, setEdit] = React.useState(false);

  const handleEdit = () => {
    setEdit(true);
  };
  const handleEditClose = (e) => {
    setEdit(false);
  };

  const [del, setDel] = React.useState(false);
  const [studentid,setstudentid]=React.useState("");


  const handleDel = (id) => {
    setDel(true);
    setstudentid(id)
  };
  const handleDelClose = () => {
    setDel(false);
  };

  return (
    <div>
      <div style={{ position: "sticky" }}>
        <Button variant="contained" color="secondary" onClick={handleClickOpen}>
          <AddCircleOutlineRoundedIcon />
          New
        </Button>
      </div>
      {/* Bulk user Creation */}
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Masters"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {/* Bulk User creation */}
            <BulkUser_Creation />
            {/*  */}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary">
            Close
          </Button>
        </DialogActions>
      </Dialog>

      {/* Edit user */}
      <Dialog
        open={edit}
        handleEditClose={handleEditClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Edit User"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <BulkUser_Creation />
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleEdit} color="primary">
            Conform
          </Button>
          <Button onClick={handleEditClose} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>

      {/* Delete User */}

      <Dialog
        open={del}
        onClose={handleDelClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Delete User"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Are you sure you want to Delete User Permanently
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={()=>handleAgree(studentid)}
           color="secondary">
            Agree
          </Button>
          <div className="spinner" />
          <Button onClick={handleDelClose} color="secondary" autoFocus>
            DisAgree
          </Button>
        </DialogActions>
      </Dialog>

      <CustomTable
        title=""
        data={allStudents}
        const
        columns={[
          {
            title: "NAME",
            field: "name",
            dataType: "String",
          },
          {
            title: "Gender",
            field: "gender",
            dataType: "String",
          },
          {
            title: "Email",
            field: "email",
            dataType: "String",
          },
          {
            title: "Department",
            field: "department",
            dataType: "String",
          },
          {
            title: "Hall Ticket No.",
            field: "id",
            defaultSort: "asc",
            render: (rowData) => {
              return (
                <Link
                  style={{ color: "blue" }}
                  to={"/console/studentlist/" + rowData.studentid}
                >
                  {rowData.studentid}
                </Link>
              );
            },
            dataType: "String",
          },
          {
            title: "Branch",
            field: "branch",
            dataType: "String",
          },
          {
            title: "Phone",
            field: "phone",
            dataType: "String",
          },

          {
            title: "Status",
            field: "status",
            dataType: "String",
            render: (rowData) => {
              if (rowData.status === "Active")
                return (
                  <span className="chip chip-success">{rowData.status}</span>
                );
              else if (rowData.status === "Inactive")
                return (
                  <span className="chip chip-pending">{rowData.status}</span>
                );
              else
                return (
                  <span className="chip chip-rejected">{rowData.status}</span>
                );
            },
            customFilterAndSearch: (value, rowData) => {
              if (value.length === 0) return true;
              return value.includes(rowData.status.toString());
            },
          },
          {
            title: "Actions",
            field: "",
            render: (rowData) => {
              return (
                <div style={{ display: "flex" }}>
                  <Button variant="outlined" color="primary">
                    <VisibilityTwoToneIcon />
                  </Button>
                  <Button
                    variant="outlined"
                    color="primary"
                    onClick={handleEdit}
                  >
                    <CreateTwoToneIcon />
                  </Button>

                  <Button
                    variant="outlined"
                    color="primary"
                    onClick={()=>handleDel(rowData.studentid)}
                  >
                    <DeleteOutlineTwoToneIcon />
                  </Button>
                </div>
              );
            },
          },
        ]}
      />
    </div>
  );
}
