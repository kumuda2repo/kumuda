import React from "react";
import {
  Button,
  TextField,
  FormLabel,
  FormControlLabel,
  RadioGroup,
  Radio,
} from "@material-ui/core";
import CheckIcon from "@material-ui/icons/Check";
import ClearIcon from "@material-ui/icons/Clear";
import { green, red } from "@material-ui/core/colors";
import Autocomplete from "@material-ui/lab/Autocomplete";
//import MaterialTable from "material-table";
import { CustomField } from "../../../../SharedComponents/FormFeilds";
import CustomTable from '../../../../SharedComponents/MaterialTable'
function Presentation(props) {
  const {
    users,
    isChangedStatus,
    handleChange,
    showdetails,
    handleUser,
    handleConditions,
    handleRole,
    handleCancel,
    handleSave,
    changedStatus,
  } = props;
  console.log(props);

  return (
    <div className="WorkflowAndRules" style={{width:"720px",bottom:"20px"}}>
        <div>
          <div className="d-flex justify-content-center">
            <FormLabel style={{ marginBottom: "auto", marginTop: "auto" }}>
              Please Select User or Role:
            </FormLabel>{" "}
            <RadioGroup
              row
              onChange={handleUser}
              value={showdetails}
              name="displayDetails"
              defaultValue="top"
            >
              <FormControlLabel
                control={<Radio color="primary" />}
                value="user"
                label="User"
              />
              <FormControlLabel
                control={<Radio color="primary" />}
                value="role"
                label="Role"
              />
            </RadioGroup>
          </div>
          {showdetails === "role" ? (
            <CustomField CustomField
            type="select"
              label="Role"
              name="selectedRole"
              handleChange={handleRole}
              required
              fullWidth
              size="small"
              menuItems={["Faculty", "Dean", "Student", "HOD"]}
            >
            </CustomField>
          ) : showdetails === "user" ? (
            <Autocomplete
              id="combo-box-demo"
              required
              options={users}
              getOptionLabel={(option) => option.name}
              onChange={(e, v) => {
                if (v) {
                  handleChange(v.name, v.entryBy, v.authorise, v.approve);
                }
              }}
              filterSelectedOptions
              renderInput={(params) => (
                <TextField
                  {...params}
                  inputProps={{
                    ...params.inputProps,
                    autoComplete: "new-password", // disable autocomplete and autofill
                  }}
                  fullWidth
                  label="Select User"
                  size="small"
                />
              )}
            />
          ) : null}
          {isChangedStatus.selectedUser === "" &&
          isChangedStatus.selectedRole === "" ? null : (
            <>
              <div className="text-center">
                <FormControlLabel
                  control={
                    <CustomField
                    type="checkbox"
                      value={isChangedStatus.entryBy}
                      handleChange={handleConditions}
                      name="entryBy"
                    />
                  }
                  label="entryBy"
                />
                <FormControlLabel
                  control={
                    <CustomField
                    type="checkbox"
                      value={isChangedStatus.authorise}
                      handleChange={handleConditions}
                      name="authorise"
                    />
                  }
                  label="Authorise"
                />
                <FormControlLabel
                  control={
                    <CustomField
                    type="checkbox"
                      value={isChangedStatus.approve}
                      handleChange={handleConditions}
                      name="approve"
                    />
                  }
                  label="Approve"
                />
              </div>
              <div className="text-center">
                <Button
                  id="workflow_save"
                  variant="contained"
                  onClick={handleSave}
                  color="secondary"
                >
                  Save
                </Button>{" "}
                <Button
                  id="workflow_cancel"
                  variant="contained"
                  onClick={handleCancel}
                  color="primary"
                >
                  Cancel
                </Button>
              </div>
            </>
          )}
        </div>
       
          <CustomTable
            title="Workflow and Rules"
            columns={[
              {
                title: "Name/Role",
                field: "name",
              },
              {
                title: "entryBy",
                field: "entryBy",
                render: (rowData) => (
                  <>
                    {rowData.entryBy ? (
                      <CheckIcon style={{ color: green[500] }} />
                    ) : (
                      <ClearIcon style={{ color: red[500] }} />
                    )}
                  </>
                ),
              },
              {
                title: "Authorise",
                field: "authorise",
                render: (rowData) => (
                  <>
                    {rowData.authorise ? (
                      <CheckIcon style={{ color: green[500] }} />
                    ) : (
                      <ClearIcon style={{ color: red[500] }} />
                    )}
                  </>
                ),
              },
              {
                title: "Approve",
                field: "approve",
                render: (rowData) => (
                  <>
                    {rowData.approve ? (
                      <CheckIcon style={{ color: green[500] }} />
                    ) : (
                      <ClearIcon style={{ color: red[500] }} />
                    )}
                  </>
                ),
              },
            ]}
            data={changedStatus.map((item) => {
              return {
                name:
                  item.selectedUser === ""
                    ? item.selectedRole
                    : item.selectedUser,
                entryBy: item.entryBy,
                authorise: item.authorise,
                approve: item.approve,
              };
            })}
           
          />
       
     
    </div>
  );
}
export default Presentation;
