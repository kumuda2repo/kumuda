import React, { Component } from "react";
import Presentation from "./Presentation";
import { connect } from "react-redux"
import { _workflowandRules } from "../../../middleware/middleware"

class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: props.workflowandRulesUsers,
      showdetails: "",
      changedStatus: [],
      isChanging: "",
      isChangedStatus: {
        selectedUser: "",
        selectedRole: "",
        entryBy: false,
        authorise: false,
        approve: false,
      }
    }
  }
  componentDidMount() {
    console.log(this.props)
    this.props.workflowandRules()
  }

  componentWillReceiveProps(nextProps) {
    console.log(nextProps)
    this.setState({
      users: nextProps.workflowandRulesUsers
    })
  }


  handleUser = (e) => {
    this.setState({
      showdetails: e.target.value
    })
    if (e.target.value === "user") {
      this.setState({
        isChanging: "user",
        isChangedStatus: {
          selectedUser: "",
          selectedRole: "",
          entryBy: false,
          authorise: false,
          approve: false,
        }
      })
    }
    if (e.target.value === "role") {
      this.setState({
        isChanging: "role",
        isChangedStatus: {
          selectedUser: "",
          selectedRole: "",
          entryBy: false,
          authorise: false,
          approve: false,
        }
      })
    }
  }

  handleRole = (e) => {
    this.setState({
      isChangedStatus: {
        ...this.state.isChangedStatus,
        selectedRole: e.target.value
      }
    })
  }

  handleConditions = (e) => {
    console.log(e.target.name, e.target.checked)
    this.setState({
      isChangedStatus: {
        ...this.state.isChangedStatus,
        [e.target.name]: e.target.value
      }
    })
  }

  handleChange = (value, entryBy, authorise, approve) => {
    console.log(value)
    this.setState({
      isChangedStatus: {
        ...this.state.isChangedStatus,
        selectedUser: value,
        entryBy: entryBy,
        authorise: authorise,
        approve: approve,
      }
    })
  };

  handleSave = () => {
    this.setState({
      changedStatus: [
        ...this.state.changedStatus,
        this.state.isChangedStatus
      ],
      showdetails: "",
      isChangedStatus: {
        selectedUser: "",
        selectedRole: "",
        entryBy: false,
        authorise: false,
        approve: false,
      }
    })
  }

  handleCancel = () => {
    this.setState({
      showdetails: "",
      isChangedStatus: {
        selectedUser: "",
        selectedRole: "",
        entryBy: false,
        authorise: false,
        approve: false,
      }
    })
  }


  render() {
    return (
      <div>
        <Presentation
          {...this.state}
          handleChange={this.handleChange}
          handleUser={this.handleUser}
          handleRole={this.handleRole}
          handleConditions={this.handleConditions}
          handleCancel={this.handleCancel}
          handleSave={this.handleSave}
        />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    workflowandRulesUsers: state.usermanagement.workflowandRulesUsers
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    workflowandRules: () => {
      dispatch(_workflowandRules())
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Container);
