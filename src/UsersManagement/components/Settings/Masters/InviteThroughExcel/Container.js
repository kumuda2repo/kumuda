import React, { Component } from "react";
import Presentation from "./Presentation";
import readXlsxFile from "read-excel-file";
import { _bulkusers } from "../../../../middleware/middleware";
import { connect } from "react-redux";

export class Container extends Component {
  constructor() {
    super();
    this.state = {
      usersarray: [],
      fileName: "",
      open: null,
      file: null,
      color: "",
      auth_create: [],
      history_payload: [],
      authCreateDummy: {
        email: "",
        emailVerified: false,
        password: "",
        displayName: "",
        uid: "",
        disabled: false,
      },
      history_payload_dummy: {
        actionOn: "",
        action: " ",
        date: new Date().toISOString(),
        actionBy: "",
      },
    };
  }

  handleClickOpen = () => {
    this.setState({
      open: true,
    });
  };

  handleFileChange = (e) => {
    e.target.files
      ? this.setState({ file: true })
      : this.setState({ file: false });
    let filename = e.target.files[0].name;
    this.setState({
      fileName: filename,
      color: "green",
    });
    readXlsxFile(e.target.files[0]).then((rows) => {
      var objs = rows.map(function (x) {
        return {
          name: x[0],
          role: x[1],
          uid: x[2],
          gender: x[3],
          email: x[4],
          phone: x[5],
          department: x[6],
          status: x[7],
          qualification: x[8],
          password: x[9],
        };
      });

      objs.splice(0, 1);

      this.setState({
        usersarray: objs,
      });
      this.state.usersarray.map((user, index) => {
        console.log(user.email);
        let dummyarray = [];
        let dummyhistory_payload = [];
        this.setState({
          authCreateDummy: {
            ...this.state.authCreateDummy,
            email: user.email,
            password: user.password,
            displayName: user.name,
            uid: user.uid,
          },
          history_payload_dummy: {
            ...this.state.history_payload_dummy,
            actionOn: user.email,
            action: `${user.email} is Successfully Created`,
            actionBy: this.props.loggedIn_UserEmail,
          },
        });
        dummyarray = this.state.authCreateDummy;
        dummyhistory_payload = this.state.history_payload_dummy;
        this.state.auth_create.push(dummyarray);
        this.state.history_payload.push(dummyhistory_payload);
      });
    });
  };

  handleClose = () => {
    this.setState({
      open: false,
      fileName: "",
      file: null,
      color: "",
    });
  };

  handleCreate = () => {
    if (this.state.file === false || this.state.file === null) {
      this.setState({
        fileName: "Choose File",
        color: "red",
      });
    }
    if (this.state.file === true) {
      this.props.bulkUser(
        this.state.usersarray,
        this.state.auth_create,
        this.state.history_payload
      );
      this.setState({
        fileName: "",
        open: false,
        file: null,
        color: "",
      });
    }
  };

  render() {
    return (
      <div>
        <Presentation
          open={this.state.open}
          color={this.state.color}
          handleFileChange={this.handleFileChange}
          fileName={this.state.fileName}
          handleCreate={this.handleCreate}
          handleClickOpen={this.handleClickOpen}
          handleClose={this.handleClose}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    loggedIn_UserEmail: state.auth.user.email,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    bulkUser: (user, auth_create, history_payload) => {
      dispatch(_bulkusers(user, auth_create, history_payload));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
