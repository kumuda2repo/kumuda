import React from "react";
import { ImFolderUpload } from "react-icons/im";
import { makeStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from "@material-ui/core";
const useStylesFacebook = makeStyles({
  root: {
    position: "relative",
    marginTop: "4px",
    marginRight: "3px",
  },
  top: {
    color: "#eef3fd",
  },
  bottom: {
    color: "#6798e5",
    animationDuration: "550ms",
    position: "absolute",
    left: 0,
  },
});

function FacebookProgress(props) {
  const classes = useStylesFacebook();

  return (
    <div className={classes.root}>
      <CircularProgress
        variant="determinate"
        value={100}
        className={classes.top}
        size={15}
        thickness={4}
        {...props}
      />
      <CircularProgress
        variant="indeterminate"
        disableShrink
        className={classes.bottom}
        size={15}
        thickness={4}
        {...props}
      />
    </div>
  );
}

function Presentation(props) {
  // const [open, setOpen] = React.useState(false);

  // const handleClickOpen = () => {
  //   setOpen(true);
  // };

  // const handleClose = () => {
  //   setOpen(false);
  // };

  const {
    handleFileChange,
    fileName,
    handleCreate,
    handleClickOpen,
    handleClose,
    open,
    color,
  } = props;
  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        Invite Multiple
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Upload Your Excel Sheet Here"}
        </DialogTitle>

        <DialogContent>
          <div>
            <label
              className="c-pointer"
              style={{
                border: "1px solid gray",
                padding: "6px",
                borderRadius: " 10px",
              }}
              htmlFor="contained-button-file-excel-invite"
            >
              <ImFolderUpload size={22} />{" "}
              <b style={{ margin: "auto" }}>Upload</b>
            </label>
            <input
              type="file"
              variant="outlined"
              className="d-none"
              id="contained-button-file-excel-invite"
              accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
              onChange={handleFileChange}
              size="small"
            />
            <br />
            <br />
            <div>
              <b style={{ color: color }}>{fileName}</b>
            </div>
          </div>
        </DialogContent>

        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleCreate} color="primary" autoFocus>
            Create
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default Presentation;
