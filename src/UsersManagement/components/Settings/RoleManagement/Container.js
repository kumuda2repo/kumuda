import React, { Component } from "react";
import Presentation from "./Presentation";
import { connect } from "react-redux";
// import {  _settings } from "../../../middleware/middleware";
import {_RoleManagement} from '../../../middleware/middleware'
class Container extends Component {
  constructor(props) {
    super(props);
    console.log(props.RoleManagementRoles);

    this.state = {
      users: props.RoleManagementRoles,
      showdetails: "",
      changedStatus: [],
      isChanging: "",
      isChangedStatus: {
        selectedUser: "",
        selectedRole: "",
        Faculty: false,
        Student: false,
        Dean: false,
        HOD: false,
      },
    };
  }

  componentDidMount() {
    console.log(this.props);
    this.props.RoleManagement();
  }

  componentWillReceiveProps(nextProps) {
    console.log(nextProps);
    this.setState({
      users: nextProps.RoleManagementRoles,
    });
  }

  handleUser = (e) => {
    this.setState({
      showdetails: e.target.value,
    });
    if (e.target.value === "user") {
      this.setState({
        isChanging: "user",
        isChangedStatus: {
          selectedUser: "",
          Faculty: false,
          Student: false,
          Dean: false,
          HOD: false
        },
      });
    }
   
  };


  handleConditions = (e) => {
    console.log(e.target.name, e.target.checked);
    this.setState({
      isChangedStatus: {
        ...this.state.isChangedStatus,
        [e.target.name]: e.target.value,
      },
    });
  };

  handleChange = (value, Faculty, Student,Dean, HOD) => {
    // console.log(value)
    this.setState({
      isChangedStatus: {
        ...this.state.isChangedStatus,
        selectedUser: value,
        Faculty: Faculty,
        Student: Student,
        Dean: Dean,
        HOD: HOD
      },
    });
  };

  handleSave = () => {
    this.setState({
      changedStatus: [...this.state.changedStatus, this.state.isChangedStatus],
      showdetails: "",
      isChangedStatus: {
        selectedUser: "",
        Faculty: false,
        Student: false,
        Dean: false,
        HOD: false
      },
    });
  };

  handleCancel = () => {
    this.setState({
      showdetails: "",
      isChangedStatus: {
        selectedUser: "",
        Faculty: false,
        Student: false,
        Dean: false,
        HOD: false
      },
    });
  };

  render() {
    console.log(this.props.premissionUsers, this.state.users);
    return (
      <div>
        <Presentation
          {...this.state}
          handleChange={this.handleChange}
          handleUser={this.handleUser}
          handleConditions={this.handleConditions}
          handleCancel={this.handleCancel}
          handleSave={this.handleSave}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    RoleManagementRoles: state.usermanagement.RoleManagementRoles,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    RoleManagement: () => {
      dispatch( _RoleManagement());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
