import React from "react";
import {
  Button,
  TextField,
  FormControlLabel,
} from "@material-ui/core";
import CheckIcon from "@material-ui/icons/Check";
import ClearIcon from "@material-ui/icons/Clear";
import { green, red } from "@material-ui/core/colors";
import Autocomplete from "@material-ui/lab/Autocomplete";
//import MaterialTable from "material-table";
//import SettingsUsers from "../SettingsUsers";
//import WorkflowAndRules from "../WorkflowAndRules";
//import Permissions from "../Permissions";
//import "../../../../Styles/Permissions.css";
import { CustomField } from "../../../../SharedComponents/FormFeilds";
import CustomTable from "../../../../SharedComponents/MaterialTable";
function Presentation(props) {
  const {
    users,
    isChangedStatus,
    handleChange,
    handleConditions,
    handleCancel,
    handleSave,
    changedStatus,
  } = props;
  console.log(props);

  return (
    <div style={{ width: "720px", height: "600px" }}>
      <div>
        <div className="d-flex justify-content-center"></div>
        {
          <Autocomplete
            id="combo-box-demo"
            required
            options={users}
            getOptionLabel={(option) => option.name}
            onChange={(e, v) => {
              if (v) {
                handleChange(v.name, v.Faculty, v.Student, v.Dean, v.HOD);
              }
            }}
            filterSelectedOptions
            renderInput={(params) => (
              <TextField
                {...params}
                inputProps={{
                  ...params.inputProps,
                  autoComplete: "new-password", // disable autocomplete and autofill
                }}
                fullWidth
                label="Select Role"
                size="small"
              />
            )}
          />
        }
        {
          <>
            <div className="text-center">
              {/*src\SharedComponents\FormFeilds.js */}
              <FormControlLabel
                control={
                  <CustomField
                    type="checkbox"
                    value={isChangedStatus.Faculty}
                    handleChange={handleConditions}
                    name="Faculty"
                  />
                }
                label="Faculty"
              />
              <FormControlLabel
                control={
                  <CustomField
                    type="checkbox"
                    value={isChangedStatus.Student}
                    handleChange={handleConditions}
                    name="Student"
                  />
                }
                label="Student"
              />
              <FormControlLabel
                control={
                  <CustomField
                    type="checkbox"
                    value={isChangedStatus.Dean}
                    handleChange={handleConditions}
                    name="Dean"
                  />
                }
                label="Dean"
              />
              <FormControlLabel
                control={
                  <CustomField
                    type="checkbox"
                    value={isChangedStatus.HOD}
                    handleChange={handleConditions}
                    name="HOD"
                  />
                }
                label="HOD"
              />
            </div>
            <div className="text-center">
              <Button
                variant="contained"
                onClick={handleSave}
                color="secondary"
              >
                Save
              </Button>{" "}
              <Button
                variant="contained"
                onClick={handleCancel}
                color="primary"
              >
                Cancel
              </Button>
            </div>
          </>
        }
      </div>

      <CustomTable
        title="RoleManagement"
        columns={[
          {
            title: "Name/Role",
            field: "name",
          },
          {
            title: "Faculty",
            field: "Faculty",
            render: (rowData) => (
              <>
                {rowData.Faculty ? (
                  <CheckIcon style={{ color: green[500] }} />
                ) : (
                  <ClearIcon style={{ color: red[500] }} />
                )}
              </>
            ),
          },
          {
            title: "Student",
            field: "Student",
            render: (rowData) => (
              <>
                {rowData.Student ? (
                  <CheckIcon style={{ color: green[500] }} />
                ) : (
                  <ClearIcon style={{ color: red[500] }} />
                )}
              </>
            ),
          },
          {
            title: "Dean",
            field: "Dean",
            render: (rowData) => (
              <>
                {rowData.Dean ? (
                  <CheckIcon style={{ color: green[500] }} />
                ) : (
                  <ClearIcon style={{ color: red[500] }} />
                )}
              </>
            ),
          },
          {
            title: "HOD",
            field: "HOD",
            render: (rowData) => (
              <>
                {rowData.HOD ? (
                  <CheckIcon style={{ color: green[500] }} />
                ) : (
                  <ClearIcon style={{ color: red[500] }} />
                )}
              </>
            ),
          },
        ]}
        data={changedStatus.map((item) => {
          return {
            name:
              item.selectedUser === "" ? item.selectedRole : item.selectedUser,
            Faculty: item.Faculty,
            Student: item.Student,
            Dean: item.Dean,
            HOD: item.HOD,
          };
        })}
        options={{
          search: true,
        }}
      />
    </div>
  );
}

export default Presentation;
