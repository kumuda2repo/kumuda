import React, { Component } from "react";
import Presentation from "./Presentation";
import { connect } from "react-redux";
import {  _settings } from "../../../middleware/middleware";

class Container extends Component {
  constructor(props) {
    super(props);
    console.log(props.settingsUsers);
    this.state = {
      users: props.settingsUsers,
      showdetails: "",
      changedStatus: [],
      isChanging: "",
      isChangedStatus: {
        selectedUser: "",
        selectedRole: "",
        cse: false,
        ece: false,
        it: false,
        ce: false,
        me: false,
        mba: false,
      },
    };
  }

  componentDidMount() {
    console.log(this.props);
    this.props.settings();
  }

  componentWillReceiveProps(nextProps) {
    console.log(nextProps);
    this.setState({
      users: nextProps.settingsUsers,
    });
  }

  handleUser = (e) => {
    this.setState({
      showdetails: e.target.value,
    });
    if (e.target.value === "user") {
      this.setState({
        isChanging: "user",
        isChangedStatus: {
          selectedUser: "",
          cse: false,
          ece: false,
          it: false,
          ce: false,
          me: false,
          mba: false,
        },
      });
    }
   
  };


  handleConditions = (e) => {
    console.log(e.target.name, e.target.checked);
    this.setState({
      isChangedStatus: {
        ...this.state.isChangedStatus,
        [e.target.name]: e.target.value,
      },
    });
  };

  handleChange = (value, cse, ece,it, ce,me,mba,e) => {
    // console.log(value)
    this.setState({
      isChangedStatus: {
        ...this.state.isChangedStatus,
        selectedUser: value,
        cse: cse,
        ece: ece,
        it: it,
        ce: ce,
        me: me,
        mba: mba,
      },
      [e.target.name]:e.target.value
    });
  };

  handleSave = () => {
    this.setState({
      changedStatus: [...this.state.changedStatus, this.state.isChangedStatus],
      showdetails: "",
      isChangedStatus: {
        selectedUser: "",
        cse: false,
        ece: false,
        it: false,
        ce: false,
        me: false,
        mba: false,
      },
    });
  };

  handleCancel = () => {
    this.setState({
      showdetails: "",
      isChangedStatus: {
        selectedUser: "",
        cse: false,
        ece: false,
        it: false,
        ce: false,
        me: false,
        mba: false,
      },
    });
  };

  render() {
    console.log(this.props.premissionUsers, this.state.users);
    return (
      <div>
        <Presentation
          {...this.state}
          handleChange={this.handleChange}
          handleUser={this.handleUser}
          handleConditions={this.handleConditions}
          handleCancel={this.handleCancel}
          handleSave={this.handleSave}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    settingsUsers: state.usermanagement.settingsUsers,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    settings: () => {
      dispatch( _settings());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
