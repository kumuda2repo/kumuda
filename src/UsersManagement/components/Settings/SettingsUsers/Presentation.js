import React from "react";
import {
  Button,
  TextField,
  FormControlLabel,
} from "@material-ui/core";
import CheckIcon from "@material-ui/icons/Check";
import ClearIcon from "@material-ui/icons/Clear";
import { green, red } from "@material-ui/core/colors";
import Autocomplete from "@material-ui/lab/Autocomplete";
//import MaterialTable from "material-table";
import "../../../../Styles/Permissions.css"
import { CustomField } from '../../../../SharedComponents/FormFeilds'
import CustomTable from '../../../../SharedComponents/MaterialTable'
function Presentation(props) {
  const {
    users,
    isChangedStatus,
    handleChange,
    handleConditions,
    handleCancel,
    handleSave,
    changedStatus,
  } = props;
  console.log(props);

  return (
    <div className="SettingsUsers" style={{ width: "720px", bottom: "40px", }}>

      <div>
        <div className="d-flex justify-content-center"></div>
        {
          <Autocomplete
            id="combo-box-demo"
            required
            options={users}
            getOptionLabel={(option) => option.name}
            onChange={(e, v) => {
              if (v) {
                handleChange(v.name, v.cse, v.ece, v.it, v.ce, v.me, v.mba);
              }
            }}
            filterSelectedOptions
            renderInput={(params) => (
              <TextField
                {...params}
                inputProps={{
                  ...params.inputProps,
                  autoComplete: "new-password", // disable autocomplete and autofill
                }}
                fullWidth
                label="Select User"
                size="small"
              />
            )}
          />
        }
        {
          <>
            <div className="text-center">
              {/*src\SharedComponents\FormFeilds.js */}
              <FormControlLabel
                control={
                  <CustomField
                    type="checkbox"
                    value={isChangedStatus.cse}
                    handleChange={handleConditions}
                    name="cse"
                  />
                }
                label="CSE"
              />
              <FormControlLabel
                control={
                  <CustomField
                    type="checkbox"
                    value={isChangedStatus.ece}
                    handleChange={handleConditions}
                    name="ece"
                  />
                }
                label="ECE"
              />
              <FormControlLabel
                control={
                  <CustomField
                    type="checkbox"
                    value={isChangedStatus.it}
                    handleChange={handleConditions}
                    name="it"
                  />
                }
                label="IT"
              />
              <FormControlLabel
                control={
                  <CustomField
                    type="checkbox"
                    value={isChangedStatus.ce}
                    handleChange={handleConditions}
                    name="ce"
                  />
                }
                label="CE"
              />
              <FormControlLabel
                control={
                  <CustomField
                    type="checkbox"
                    value={isChangedStatus.me}
                    handleChange={handleConditions}
                    name="me"
                  />
                }
                label="ME"
              />
              <FormControlLabel
                control={
                  <CustomField
                    type="checkbox"
                    value={isChangedStatus.mba}
                    handleChange={handleConditions}
                    name="mba"
                  />
                }
                label="MBA"
              />
            </div>
            <div className="text-center">
              <Button
                variant="contained"
                onClick={handleSave}
                color="secondary"
              >
                Save
              </Button>{" "}
              <Button
                variant="contained"
                onClick={handleCancel}
                color="primary"
              >
                Cancel
              </Button>
            </div>
          </>
        }
      </div>

      <CustomTable
        title="Settings"
        columns={[
          {
            title: "Name/Role",
            field: "name",
          },
          {
            title: "CSE",
            field: "cse",
            render: (rowData) => (
              <>
                {rowData.cse ? (
                  <CheckIcon style={{ color: green[500] }} />
                ) : (
                    <ClearIcon style={{ color: red[500] }} />
                  )}
              </>
            ),
          },
          {
            title: "ECE",
            field: "ece",
            render: (rowData) => (
              <>
                {rowData.ece ? (
                  <CheckIcon style={{ color: green[500] }} />
                ) : (
                    <ClearIcon style={{ color: red[500] }} />
                  )}
              </>
            ),
          },
          {
            title: "IT",
            field: "it",
            render: (rowData) => (
              <>
                {rowData.it ? (
                  <CheckIcon style={{ color: green[500] }} />
                ) : (
                    <ClearIcon style={{ color: red[500] }} />
                  )}
              </>
            ),
          },
          {
            title: "CE",
            field: "ce",
            render: (rowData) => (
              <>
                {rowData.ce ? (
                  <CheckIcon style={{ color: green[500] }} />
                ) : (
                    <ClearIcon style={{ color: red[500] }} />
                  )}
              </>
            ),
          },
          {
            title: "ME",
            field: "me",
            render: (rowData) => (
              <>
                {rowData.me ? (
                  <CheckIcon style={{ color: green[500] }} />
                ) : (
                    <ClearIcon style={{ color: red[500] }} />
                  )}
              </>
            ),
          },
          {
            title: "MBA",
            field: "mba",
            render: (rowData) => (
              <>
                {rowData.mba ? (
                  <CheckIcon style={{ color: green[500] }} />
                ) : (
                    <ClearIcon style={{ color: red[500] }} />
                  )}
              </>
            ),
          },
        ]}
        data={changedStatus.map((item) => {
          return {
            name:
              item.selectedUser === ""
                ? item.selectedRole
                : item.selectedUser,
            cse: item.cse,
            ece: item.ece,
            it: item.it,
            ce: item.ce,
            me: item.me,
            mba: item.mba,
          };
        })}

      />

    </div>
  );
}

export default Presentation;
