import React from "react";
import PropTypes from "prop-types";
import SwipeableViews from "react-swipeable-views";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import ProfileTemplates from "../TemplatesSection/ProfileTemplates";
 import RoleManagement from "../RoleManagement";
 import Permissions from '../Permissions'
 import WorkflowAndRules from '../WorkflowAndRules'
 import SettingsUsers from '../SettingsUsers'
import EmailTemplates from "../EmailTemplates/Index";
import {
  AppBar,
  Tabs,
  Tab,
  Typography,
  Box,
  Paper,
}
 from "@material-ui/core";


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}
TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};
function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    "aria-controls": `full-width-tabpanel-${index}`,
  };
}
// app bar styles
const useStyles = makeStyles((theme) => ({
  demo1: {
    backgroundColor: "#fffeff",
    boxShadow: "none",
  },
}));

function Presentation() {
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  return (
    <div>
      <br />
      {/* titlebar */}
      <div>
        <AppBar position="static" color="default" className={classes.demo1}>
          <Tabs
            value={value}
            onChange={handleChange}
            indicatorColor="primary"
            textColor="primary"
            variant="scrollable"
            scrollButtons="auto"
          >
            <Tab label="Profile Templates" {...a11yProps(0)} />
            <Tab label="Email Templates" {...a11yProps(1)} />
            <Tab label="Permissions" {...a11yProps(2)} />
          
          </Tabs>
        </AppBar>
        <br />

        <SwipeableViews
          axis={theme.direction === "rtl" ? "x-reverse" : "x"}
          index={value}
          onChangeIndex={handleChangeIndex}
        >
         

          {/* first tab */}
          <Paper elevation={2}>
            <TabPanel value={value} index={0} dir={theme.direction}>
              <ProfileTemplates />
            </TabPanel>
          </Paper>
          {/* second tab */}
          <Paper elevation={2}>
            <TabPanel value={value} index={1} dir={theme.direction}>
              <EmailTemplates />
            </TabPanel>
          </Paper>
          {/*third tab */}
         
            <TabPanel value={value} index={2} dir={theme.direction}>

              <div style={{width:"400px",marginBottom: "100px"}}>
                <RoleManagement/>
              </div>
              <div style={{marginLeft:"800px",top:"25px",position:"fixed",}}>
              <SettingsUsers/>
              </div>
               <div style={{width: "400px",marginBottom:"100px",}}>
              <WorkflowAndRules/>
               </div>
               <div style={{top:"726px",position:"fixed",marginLeft:"800px",width:"400px"}}>
                <Permissions/>
               </div>

            </TabPanel>
        </SwipeableViews>
      </div>
    </div>
  );
}

export default Presentation;
