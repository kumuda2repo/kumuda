import React, { Component } from "react";
import Presentation from "./Presentation";
import { connect } from "react-redux"
import { _premissions } from "../../../middleware/middleware"


class Container extends Component {
  constructor(props) {
    super(props);
    console.log(props.premissionUsers)
    this.state = {
      users: props.premissionUsers,
      showdetails: "",
      changedStatus: [],
      isChanging: "",
      isChangedStatus: {
        selectedUser: "",
        selectedRole: "",
        create: false,
        update: false,
        read: false,
        delete: false
      }
    }
  }
  componentDidMount() {
    console.log(this.props)
    this.props.premissions()
  }

  componentWillReceiveProps(nextProps) {
    console.log(nextProps)
    this.setState({
      users: nextProps.premissionUsers
    })
  }

  handleUser = (e) => {
    this.setState({
      showdetails: e.target.value
    })
    if (e.target.value === "user") {
      this.setState({
        isChanging: "user",
        isChangedStatus: {
          selectedUser: "",
          selectedRole: "",
          create: false,
          update: false,
          read: false,
          delete: false
        }
      })
    }
    if (e.target.value === "role") {
      this.setState({
        isChanging: "role",
        isChangedStatus: {
          selectedUser: "",
          selectedRole: "",
          create: false,
          update: false,
          read: false,
          delete: false
        }
      })
    }
  }

  handleRole = (e) => {
    this.setState({
      isChangedStatus: {
        ...this.state.isChangedStatus,
        selectedRole: e.target.value
      }
    })
  }

  handleConditions = (e) => {
    console.log(e.target.name, e.target.checked)
    this.setState({
      isChangedStatus: {
        ...this.state.isChangedStatus,
        [e.target.name]: e.target.value
      }
    })
  }

  
  handleChange = (value, create, update, read, deleteValue) => {
    // console.log(value)
    this.setState({
      isChangedStatus: {
        ...this.state.isChangedStatus,
        
        selectedUser: value,
        create: create,
        update: update,
        read: read,
        delete: deleteValue,
      }
    })
  };
  
  handleSave = () => {
    this.setState({
      changedStatus: [
        ...this.state.changedStatus,
        this.state.isChangedStatus
      ],
      showdetails: "",
      isChangedStatus: {
        selectedUser: "",
        selectedRole: "",
        create: false,
        update: false,
        read: false,
      }
    })
  }

  handleCancel = () => {
    this.setState({
      showdetails: "",
      isChangedStatus: {
        selectedUser: "",
        selectedRole: "",
        create: false,
        update: false,
        read: false,
      }
    })
  }


  render() {
    console.log(this.props.premissionUsers, this.state.users)
    return (
      <div>
        <Presentation
          {...this.state}
          handleChange={this.handleChange}
          handleUser={this.handleUser}
          handleRole={this.handleRole}
          handleConditions={this.handleConditions}
          handleCancel={this.handleCancel}
          handleSave={this.handleSave}
        />
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    premissionUsers: state.usermanagement.premissionUsers
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    premissions: () => {
      dispatch(_premissions())
    }
  }
}



export default connect(mapStateToProps, mapDispatchToProps)(Container);
