import React from "react"; 
import {
  Button,
  TextField,
  FormLabel,
  FormControlLabel,
  RadioGroup,
  Radio,
} from "@material-ui/core";
import CheckIcon from "@material-ui/icons/Check";
import ClearIcon from "@material-ui/icons/Clear";  
import { green, red } from "@material-ui/core/colors"; 
import Autocomplete from "@material-ui/lab/Autocomplete"; 

import "../../../../Styles/Permissions.css";
import { CustomField } from "../../../../SharedComponents/FormFeilds";
import CustomTable from "../../../../SharedComponents/MaterialTable";
function Presentation(props) {
  const {
    users, 
    isChangedStatus, 
    handleChange, 
    showdetails,            
    handleUser, 
    handleConditions, 
    handleRole,
    handleCancel,
    handleSave,
    changedStatus,
  } = props;
  console.log(props);

  return (
    <div style={{width:"720px",}}>
          
              <div>
               <div className="d-flex justify-content-center">
                {" "}
                {/* div for user and role radio buttons */}
                <FormLabel style={{ marginBottom: "auto", marginTop: "auto" }}>
                  Please Select User or Role:
                </FormLabel>{" "}
                <RadioGroup
                  row
                  onChange={handleUser}
                  value={showdetails} 
                  name="displayDetails" 
                  defaultValue="top"
                > 
                  <FormControlLabel
                    control={<Radio color="primary" />}
                    value="user"
                    label="User"
                  />
                  <FormControlLabel
                    control={<Radio color="primary" />}
                    value="role"
                    label="Role"
                  />
                </RadioGroup>
              </div>
              
              
              <div class="permissions_details">
                {" "}
                {/* div for permossion details */}
                {showdetails === "role" ? (
                  <CustomField
                    type="select"
                    label="Role"
                    name="selectedRole"
                    handleChange={handleRole}
                    required
                    fullWidth
                    size="small"
                    menuItems={["Faculty", "Dean", "Student", "HOD"]}
                  ></CustomField>
                ) : showdetails === "user" ? (
                  <Autocomplete
                    id="combo-box-demo"
                    required
                    options={users}
                    getOptionLabel={(option) => option.name}
                    onChange={(e, v) => {
                      if (v) {
                        handleChange(
                          v.name,
                          v.create,
                          v.update,
                          v.read,
                          v.delete
                        );
                      }
                    }} 
                    filterSelectedOptions
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        inputProps={{
                          ...params.inputProps,
                          autoComplete: "new-password", // disable autocomplete and autofill
                        }}
                        fullWidth
                        label="Select User"
                        size="small"
                      />
                    )}
                  />
                ) : null}
              </div>
              {isChangedStatus.selectedUser === "" && 
              isChangedStatus.selectedRole === "" ? null : (
                <>
                  <div className="text-center">
                    <FormControlLabel
                      control={
                        <CustomField
                          type="checkbox"
                          value={isChangedStatus.create}
                          handleChange={handleConditions}
                          name="create"
                        />
                      }
                      label="create"
                    />
                    <FormControlLabel
                      control={
                        <CustomField  
                          type="checkbox"
                          value={isChangedStatus.update}
                          handleChange={handleConditions}
                          name="update"
                        />
                      }
                      label="update"
                    />
                    <FormControlLabel
                      control={
                        <CustomField
                          type="checkbox"
                          value={isChangedStatus.read} 
                          handleChange={handleConditions}
                          name="read"
                        />
                      }
                      label="read"
                    />
                    <FormControlLabel
                      control={
                        <CustomField
                          type="checkbox"
                          value={isChangedStatus.delete}
                          handleChange={handleConditions}
                          name="delete"
                        />
                      }
                      label="delete"
                    />
                  </div>
                  <div className="text-center">
                    <Button 
                      id="Permission_save"
                      variant="contained"
                      onClick={handleSave}
                      color="secondary"
                    >
                      Save
                    </Button>{" "}
                    <Button
                      id="permission_cancel"
                      variant="contained"
                      onClick={handleCancel}
                      color="primary"
                    >
                      Cancel
                    </Button>
                  </div>
                </> 
              )}
            </div>
            <div class="table_permissions" style={{}}>
              <CustomTable
                title="Permissions"
                columns={[
                  {
                    title: "Name/Role",
                    field: "name", 
                  },
                  {
                    title: "create",
                    field: "create",
                    render: (rowData) => (
                      <>
                        {rowData.create ? (
                          <CheckIcon style={{ color: green[500] }} />
                        ) : (
                          <ClearIcon style={{ color: red[500] }} />
                        )}
                      </>
                    ),
                  },
                  {
                    title: "update",
                    field: "update",
                    render: (rowData) => (
                      <> 
                        {rowData.update ? (
                          <CheckIcon style={{ color: green[500] }} />
                        ) : (
                          <ClearIcon style={{ color: red[500] }} />
                        )}
                      </>
                    ),
                  },
                  {
                    title: "read",
                    field: "read",
                    render: (rowData) => (
                      <>
                        {rowData.read ? (
                          <CheckIcon style={{ color: green[500] }} /> 
                        ) : (
                          <ClearIcon style={{ color: red[500] }} />
                        )}
                      </>
                    ),
                  },
                  {
                    title: "delete",
                    field: "delete",
                    render: (rowData) => (
                      <>
                        {rowData.delete ? (
                          <CheckIcon style={{ color: green[500] }} />
                        ) : (
                          <ClearIcon style={{ color: red[500] }} />
                        )}
                      </>
                    ),
                  },
                ]}
                data={changedStatus.map((item) => {
                  return {
                    name:
                      item.selectedUser === ""
                        ? item.selectedRole
                        : item.selectedUser,
                    create: item.create,
                    update: item.update,
                    read: item.read,
                    delete: item.delete,
                  };
                })}
                options={{
                  search: true,
                }}
              />
            </div>
            {/* <div class="button_bottom">
          <Button variant="contained" color="primary">
            Save
          </Button>
          <Button variant="contained" color="secondary" onClick={handleCancel}>
            cancel
          </Button>
        </div> */}
          </div>
     
  );
}

export default Presentation;
