import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { FaRegHandPointRight } from 'react-icons/fa'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        width: '100%',
        // flexBasis: '33.33%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
    },
    list: {
        width: '100%'
    }
}));

export default function Presentation(props) {
    const classes = useStyles();
    const { onSelect, letterTypes } = props



    return (
        <div className={classes.list}>
            {
                letterTypes.map(item => {
                    return (
                        <List selection className="c-pointer" onClick={() => onSelect(item.name)} verticalAlign='middle'>
                            <ListItem className="d-flex" >
                                <FaRegHandPointRight size={22} />
                                <ListItemText >
                                    {item.name}
                                </ListItemText>
                            </ListItem>
                        </List>

                    )
                })
            }
        </div>
    );
}
