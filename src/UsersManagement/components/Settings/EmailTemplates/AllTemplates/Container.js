import React, { Component } from 'react'
import Presentation from './Presentation'
import { connect } from "react-redux"
import { _emailTemplates, _setState } from "../../../../middleware/middleware"
import $ from 'jquery'

class Container extends Component {
  constructor(props) {
    super(props);
    console.log(props)
    this.state = {
      content: props.defaultContent,
      letterTypes: [],
      isUpdating: props.isUpdating
    }
  }

  componentDidMount = () => {
    this.props.emailTemplates()
  }

  componentWillReceiveProps = (nextProps) => {
    console.log(nextProps)
    this.setState({
      letterTypes: nextProps.letterTypes
    })
  }

  onSelect = (name) => {
    console.log(name)
    $("#template_panel_1").trigger('click')
    this.props.setState()
  }

  render() {
    console.log(this.state)
    return (
      <div>
        <Presentation
          {...this.state}
          onSelect={this.onSelect}
        />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    defaultContent: state.usermanagement.defaultContent,
    letterTypes: state.usermanagement.letterTypes,
    isUpdating: state.usermanagement.isupdatingtemplate
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    emailTemplates: () => {
      dispatch(_emailTemplates())
    },
    setState: () => {
      dispatch(_setState())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Container);
