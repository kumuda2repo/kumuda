import React, { Component } from 'react'
import Presentation from './Presentation'
import { connect } from "react-redux"

class Container extends Component {
    constructor(props) {
        super(props);
        this.state = {
            letters: [],
            value: 0
        }
    }


    handleChange = (e, v) => {
        this.setState({ value: v })
    }


    render() {

        console.log(this.props.isupdating)
        return (
            <div>
                <Presentation
                    {...this.state}
                    isupdating={this.props.isupdating}
                    handleChange={this.handleChange}
                />
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        isupdating: state.usermanagement.isupdatingtemplate
    }
}

export default connect(mapStateToProps)(Container)
