import React from 'react'
import SunEditor from 'suneditor-react';
import 'suneditor/dist/css/suneditor.min.css'; // Import Sun Editor's CSS File
import { Button, Typography } from '@material-ui/core';
import {CustomField} from "../../../../../SharedComponents/FormFeilds"
export default function Presentation(props) {
    const { handleChange, handleContent, name, isUpdating, helperText, content } = props


    return (
        <div>
            <form onSubmit="" >
                <div style={{ display: 'flex', justifyContent: 'space-between', marginBottom: 10 }}>
                    {/*src\SharedComponents\FormFeilds.js */}
                    <CustomField
                        label="Enter template name"
                        type="name"
                        handleChange={handleChange}
                        size="small"
                        value={name}
                        variant="outlined"
                        name='name'
                        required
                    />
                </div>
                {helperText.length ? <Typography color="error">Check these labels with reference labels</Typography> : null}
                <div>{helperText.map(item =>
                    <div>
                        <Typography color="error"><li>{item}</li></Typography>
                    </div>
                )
                }</div>
                <SunEditor
                    onChange={handleContent}
                    setContents={content}
                    height="300"
                    setOptions={{
                        "buttonList": [
                            [
                                "undo",
                                "redo",
                                "font",
                                "fontSize",
                                "formatBlock",
                                "paragraphStyle",
                                "blockquote",
                                "bold",
                                "underline",
                                "italic",
                                "strike",
                                "subscript",
                                "superscript",
                                "fontColor",
                                "hiliteColor",
                                "textStyle",
                                "removeFormat",
                                "outdent",
                                "indent",
                                "align",
                                "horizontalRule",
                                "list",
                                "lineHeight",
                                "table",
                                "link",
                                "image",
                                "video",
                                "fullScreen",
                                "showBlocks",
                                "codeView",
                                "preview",
                                "print",
                                "save",
                                "template"
                            ],

                        ],

                    }}


                />
                <div className="text-center mt-4">
                    {
                        isUpdating ?
                            <span>
                                {
                                    isUpdating ?
                                        <Button
                                            variant="contained"
                                            className="flairbtn"
                                            disabled
                                            type='button'
                                        >
                                            Update letter
                                        </Button> :
                                        <Button
                                            variant="contained"
                                            className="flairbtn"
                                            type='submit'
                                        >
                                            Update letter
                                        </Button>
                                }
                                {' '}
                                <Button
                                    variant="contained"
                                    color="secondary"
                                // onClick={cancelUpdate}
                                >
                                    Cancel
                                </Button>
                            </span>
                            :
                            isUpdating ?
                                null
                                :
                                <Button variant="contained" type="submit" color="primary"
                                // onClick={handleCreate}
                                >
                                    Create new template
                            </Button>
                    }
                    {/* <br /> */}
                </div>
                {/* {
                    isUpdating ?
                        <div className="text-right" >
                            <Tooltip title="Delete this template">
                                <Button
                                    variant="contained"
                                    color="secondary"
                                    type='button'
                                // onClick={handleDelete}
                                >
                                    <DeleteIcon />
                                </Button>
                            </Tooltip>
                        </div>
                        :
                        null

                } */}
            </form>
            <Typography className="mt-2"><h4><u>Note:</u></h4> Use the listed labels with '~Label~ ' format. (Use space after the Label)</Typography>
        </div>
    )
}