import React, { Component } from 'react'
import Presentation from './Presentation'
import { connect } from "react-redux"

class Container extends Component {
    constructor(props) {
        super(props);
        console.log(props)
        this.state = {
            content: props.defaultContent,
            isUpdating: props.isUpdating,
            type: "",
            name: "",
            helperText: []
        }
    }


    handleContent = (data) => {
        this.setState({ content: data });
        let str = data.toString()
        console.log(str.split(" "))
        let newArray = []
        let arr = str.split(" ")
        arr.forEach(element => {
            var tmpStr = element.match("~(.*)~");
            if (tmpStr !== null) newArray.push(tmpStr[1])
        });
    }


    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }





    render() {
        return (
            <div>
                <Presentation
                    {...this.state}
                    handleChange={this.handleChange}
                    handleContent={this.handleContent}
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        defaultContent: state.usermanagement.defaultContent,
        letterTypes: state.usermanagement.letterTypes,
        isUpdating: state.usermanagement.isupdatingtemplate
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        //   emailTemplates: () => {
        //     dispatch(_emailTemplates())
        // }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Container);
