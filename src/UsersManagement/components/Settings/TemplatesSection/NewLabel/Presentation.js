import React from "react";
import {
  Button,
  IconButton,
  Grid,
  FormLabel,
  Switch,
  Typography,
} from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import AddCircleIcon from "@material-ui/icons/AddCircle";

class Presentation extends React.Component {
  state = {
    open: false,
  };

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
    this.props.handleNewLabelCancel();
  };

  render() {
    // console.log(this.props)
    const {
      handleNewSectionChange,
      isNewSection,
      newSectionName,
      handleNewLabelChange,
      handleNewLabel,
      newLabelType,
      newLabelName,
      newLabelSection,
    } = this.props;
    // console.log(isNewSection)
    const toggle = (e) => {
      e.preventDefault();
      this.setState({ open: !this.state.open });
      handleNewLabel();
    };
    return (
      <div>
        <IconButton onClick={this.onOpenModal} style={{ float: "right" }}>
          <AddCircleIcon color="primary" />
        </IconButton>
        <Dialog
          fullWidth
          disableBackdropClick
          disableEscapeKeyDown
          open={this.state.open}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">Add a New label</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              <form onSubmit={toggle}>
                <FormControl>
                  <FormLabel>Are You Want to Add New Section</FormLabel>
                  <Typography component="div">
                    <Grid
                      component="label"
                      container
                      alignItems="center"
                      spacing={1}
                    >
                      <Grid item>No</Grid>
                      <Grid item>
                        <Switch
                          checked={isNewSection}
                          onChange={handleNewSectionChange}
                          name="isNewSection"
                        />
                      </Grid>
                      <Grid item>Yes</Grid>
                    </Grid>
                  </Typography>
                </FormControl>
                <br />
                {isNewSection ? (
                  <TextField
                    label="Enter Name of the New Section"
                    name="newSectionName"
                    fullWidth
                    required
                    size="small"
                    onChange={handleNewLabelChange}
                  />
                ) : (
                  <FormControl style={{ width: "100%" }}>
                    <InputLabel required> Select section</InputLabel>
                    <Select
                      fullWidth
                      label="Select section"
                      name="newLabelSection"
                      required
                      onChange={handleNewLabelChange}
                    >
                      <MenuItem value="">
                        <em>None</em>
                      </MenuItem>
                      <MenuItem value="personaldetails">
                        Personal Details
                      </MenuItem>
                      <MenuItem value="address">Address</MenuItem>
                      <MenuItem value="emergencycontact">
                        Emergency contact
                      </MenuItem>
                    </Select>
                  </FormControl>
                )}
                <br />
                <br />
                <TextField
                  label="Enter Name of the Field"
                  name="newLabelName"
                  fullWidth
                  required
                  size="small"
                  onChange={handleNewLabelChange}
                />
                <br />
                <br />
                <FormControl required style={{ width: "100%" }}>
                  <InputLabel required> Select Type of the Field</InputLabel>
                  <Select
                    fullWidth
                    label="Select Type of the Field"
                    name="newLabelType"
                    required
                    onChange={handleNewLabelChange}
                  >
                    <MenuItem value="">
                      <em>None</em>
                    </MenuItem>
                    <MenuItem value="text">Text</MenuItem>
                    <MenuItem value="email">Email</MenuItem>
                    <MenuItem value="number">Number</MenuItem>
                    <MenuItem value="date">Date</MenuItem>
                    <MenuItem value="address">Address</MenuItem>
                  </Select>
                </FormControl>
                <br />
                <br />
                {(newSectionName.length || newLabelSection.length) &&
                newLabelName.trim().length &&
                newLabelType.length ? (
                  <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    style={{ float: "right" }}
                  >
                    Create
                  </Button>
                ) : (
                  <Button
                    type="button"
                    variant="contained"
                    color="default"
                    disabled
                    style={{ float: "right" }}
                  >
                    Create
                  </Button>
                )}
                <Button
                  onClick={this.onCloseModal}
                  type="button"
                  variant="contained"
                  color="default"
                  style={{ float: "right" }}
                >
                  Cancel
                </Button>
              </form>
            </DialogContentText>
          </DialogContent>
          <DialogActions></DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default Presentation;
