import React, { Component } from 'react'
import Presentation from './Presentation'

class Container extends Component {
    render() {
        // console.log(this.props)
        const { handleNewSectionChange, isNewSection, newSectionName, handleNewLabelChange, handleNewLabel, newLabelType, newLabelName, newLabelSection, handleNewLabelCancel } = this.props;
        return (
            <div>
                <Presentation
                    handleNewLabelChange={handleNewLabelChange}
                    handleNewSectionChange={handleNewSectionChange}
                    newLabelSection={newLabelSection}
                    newLabelName={newLabelName}
                    newLabelType={newLabelType}
                    isNewSection={isNewSection}
                    newSectionName={newSectionName}
                    handleNewLabelCancel={handleNewLabelCancel}
                    handleNewLabel={handleNewLabel}
                />
            </div>
        )
    }
}

export default Container