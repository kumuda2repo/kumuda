import React from "react";
import { IconButton, Grid, makeStyles, Container } from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import ClearIcon from "@material-ui/icons/Clear";
import NewLabel from "../NewLabel";
import { CustomField } from "../../../../../SharedComponents/FormFeilds";
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  EditIcon: {
    color: "#1976d2",
    "&:hover": {
      color: "#1cacd7",
    },
  },
  Icon: {
    color: "#990011FF",
    "&:hover": {
      color: "#DA291CFF",
    },
  },
}));

export default function Presentation(props) {
  const classes = useStyles();
  // console.log(props)
  const {
    handleNewSectionChange,
    isNewSection,
    newSectionName,
    data,
    handleChange,
    // handleDelete,
    isEditing,
    handleFinish,
    handleEditicon,
    StudentTemplate,
    handleNewLabelChange,
    newLabelSection,
    newLabelName,
    newLabelType,
    handleNewLabelCancel,
    handleNewLabel,
  } = props;
  // console.log(newLabelSection, newLabelName, newLabelType, handleNewLabelChange)
  return (
    <div className={classes.root}>
      {!isEditing ? (
        <IconButton
          className={classes.EditIcon}
          style={{ float: "right" }}
          onClick={handleEditicon}
        >
          <EditIcon />
        </IconButton>
      ) : (
        <>
          <IconButton
            onClick={handleFinish}
            className={classes.Icon}
            style={{ float: "right" }}
          >
            <ClearIcon />
          </IconButton>
          <NewLabel
            handleNewSectionChange={handleNewSectionChange}
            handleNewLabelChange={handleNewLabelChange}
            newLabelSection={newLabelSection}
            newLabelName={newLabelName}
            newLabelType={newLabelType}
            isNewSection={isNewSection}
            newSectionName={newSectionName}
            handleNewLabelCancel={handleNewLabelCancel}
            handleNewLabel={handleNewLabel}
          />
        </>
      )}
      {

        console.log(StudentTemplate)
      }
      {StudentTemplate.map((portion) => {
        return (
          <Container fixed>
            <h3 style={{ marginBottom: "10px" }}>
              <u>{portion.name}</u>
            </h3>
            <Grid container spacing={3}>
              {portion["fields"].map((item) => {
                return (
                  <Grid item xs={6}>
                    {/*src\SharedComponents\FormFeilds.js */}
                    <CustomField
                      type={item.type}
                      name={item.name}
                      label={item.label}
                      menuItems={item.menuItems}
                      handleChange={handleChange}
                      value={data[item.name]}
                    />
                  </Grid>
                )
              })}

            </Grid>
            <br />
            <br />
          </Container>
        );
      })}
    </div>
  );
}
