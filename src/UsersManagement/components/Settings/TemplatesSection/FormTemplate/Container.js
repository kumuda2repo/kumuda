import React, { Component } from "react";
import Presentation from "./Presentation";

export default class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {
      StudentTemplate: this.props.TemplateDetails,
      isNewSection: false,
      newSectionName: "",
      newLabelSection: "",
      newLabelName: "",
      newLabelType: "",
      isEditing: false,
    };
  }
  handleChange = (e) => {
      console.log(e.target.name,e.target.value)
    this.setState({
        
      [e.target.name]: e.target.value,
    });
  };
  handleNewSectionChange = () => {
    this.setState({
      isNewSection: !this.state.isNewSection,
    });
  };

  handleEditicon = () => {
    this.setState({
      isEditing: true,
    });
  };

  handleFinish = () => {
    this.setState({
      isEditing: false,
    });
  };

  handleNewLabelChange = (e) => {
    console.log(e.target.name, e.target.value);
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleNewLabel = (e) => {
    // console.log(this.props.TemplateDetails, this.state.StudentTemplate)
    let StudentTemplate = this.state.StudentTemplate;
    // console.log(StudentTemplate)
    const section = this.state.newLabelSection;
    // console.log(this.state.newLabelName)
    const newLabelName = this.state.newLabelName
      .replace(/ /g, "")
      .toLowerCase();
    // console.log(newLabelName)
    let sections = this.state.StudentTemplate;
    // console.log(sections)
    if (this.state.isNewSection) {
      const newSectionName = this.state.newSectionName
        .replace(/ /g, "")
        .toLowerCase();
      // console.log(StudentTemplate)
      // console.log(newSectionName)
      StudentTemplate.push({
        name: this.state.newSectionName,
        id: newSectionName,
        section: newSectionName,
        fields: [
          {
            customize: true,
            isExist: true,
            label: this.state.newLabelName,
            name: newLabelName,
            type: this.state.newLabelType,
            required: true,
          },
        ],
      });
      this.setState({
        StudentTemplate: StudentTemplate,
        newLabelSection: "",
        newLabelName: "",
        newLabelType: "",
        isNewSection: false,
        newSectionName: "",
      });
    } else {
      sections.forEach((item, index) => {
        if (item.section === section) {
          if (
            item.fields.filter((field) => field.name === newLabelName)
              .length === 0
          ) {
            item["fields"].push({
              customize: true,
              isExist: true,
              label: this.state.newLabelName,
              name: newLabelName,
              type: this.state.newLabelType,
              required: true,
            });
          } else
            alert(
              "A field already exists with the given name, please choose another name."
            );
        }
      });
      this.setState({
        StudentTemplate: sections,
        newLabelSection: "",
        newLabelName: "",
        newLabelType: "",
        isNewSection: false,
        newSectionName: "",
      });
    }
  };

  handleNewLabelCancel = () => {
    this.setState({
      newLabelSection: "",
      newLabelName: "",
      newLabelType: "",
      isNewSection: false,
      newSectionName: "",
    });
  };

  handleDelete = (name, section) => {
    // console.log(name, section)
    let sections = this.state.StudentTemplate;
    sections.forEach((item, index) => {
      if (item.section === section) {
        item.fields.filter((field, index) => {
          if (field.name === name) {
            item.fields.splice(index, 1);
          }
          return {};
        });
      }
    });
    this.setState({ StudentTemplate: sections });
  };

  render() {
    // console.log(this.state)
    return (
      <div>
        <Presentation
          {...this.state}
          handleNewLabelChange={this.handleNewLabelChange}
          handleNewLabelCancel={this.handleNewLabelCancel}
          handleNewLabel={this.handleNewLabel}
          handleFinish={this.handleFinish}
          handleEditicon={this.handleEditicon}
          handleDelete={this.handleDelete}
          handleChange={this.handleChange}
          handleNewSectionChange={this.handleNewSectionChange}
          data={this.state}
       
        />
      </div>
    );
  }
}
