import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { Tabs, Tab, Typography, Box } from '@material-ui/core';
import FormTemplate from "../FormTemplate";
// import { StudentTemplate, FacultyTemplate } from "../../../../../SharedComponents/TemplateFields"

function TabPanel(props) {
    const { children, value, index,...other } = props;
    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`vertical-tabpanel-${index}`}
            aria-labelledby={`vertical-tab-${index}`}
            {...other}
        >
            {value === index && <Box p={3}>{children}</Box>}
        </Typography>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        display: 'flex',
    },
    tabs: {
        borderRight: `1px solid ${theme.palette.divider}`,
        overflow: 'visible'
    },
    grid: {
        padding: theme.spacing(10)
    },
}));

export default function Presentation(props) {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <div className={classes.root}>
            <Tabs
                orientation="vertical"
                variant="scrollable"
                value={value}
                onChange={handleChange}
                aria-label="Vertical tabs example"
                className={classes.tabs}
            >
                <Tab label="StudentTemplate" />
                <Tab label="FacultyTemplate" />
            </Tabs>
            <TabPanel value={value} index={0}>
                <FormTemplate TemplateDetails={props.studentTemplates} />
            </TabPanel>
            <TabPanel value={value} index={1}>
                <FormTemplate TemplateDetails={props.facultyTemplates} />
            </TabPanel>
        </div>
    );
}