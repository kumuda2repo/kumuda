import React, { Component } from 'react';
import Presentation from './Presentation';
import { connect } from "react-redux";
import { _studentTemplates, _facultyTemplates } from "../../../../middleware/middleware"
class Container extends Component {
  constructor(props) {
    super(props);
    this.state = {
      studentTemplates: props.studentFields,
      facultyTemplates: props.facultyFields,
    }
    console.log(this.state)
  }
  componentDidMount() {
    this.props.StudentTemplates()
    this.props.FacultyTemplates()
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      studentTemplates: nextProps.studentFields,
      facultyTemplates: nextProps.facultyFields,
    })
  }
  render() {
    return (
      <div>
        <Presentation
          {...this.state}
        />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    studentFields: state.usermanagement.studentFields,
    facultyFields: state.usermanagement.facultyFields,
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    StudentTemplates: () => dispatch(_studentTemplates()),
    FacultyTemplates: () => dispatch(_facultyTemplates())
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Container);