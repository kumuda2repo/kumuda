import React from 'react'
import CustomTable from '../../../SharedComponents/MaterialTable'
function Presentation() {
    return (
        <div>
            <CustomTable
            title="Reports"
            const columns={[
                {
                    title: "NAME",
                    field: "name",
                    dataType: "String",
                  },
                  {
                    title: "Email",
                    field: "mail",
                    dataType: "String",
                  },
                  {
                    title: "Marks",
                    field: "marks",
                    dataType: "String",
                  },
            ]}
            data={[ { name: "Praveen",mail:"praveen@gmail.com",marks:80},
            { name: "Murali",mail:"murali@gmail.com",marks:90 },
            { name: "Sowjanya",mail:"sowjanya@gmail.com",marks:95 },
            { name: "Taruna",mail:"taruna@gmail.com",marks:95},
            { name: "harika",mail:"harika@gmail.com",marks:95},
            { name: "Mahitha",mail:"mahitha@gmail.com",marks:99},]}
            />
        </div>
    )
    
}

export default Presentation
