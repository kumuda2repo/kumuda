import React, { Component } from 'react'
import Presentation from './Presentation'
export class Container extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             name:"",
             email:"",
            
        }
    }
    handleChange=(e)=>{
       this.setState({
           [e.target.name] :e.target.value
       })
    }
    render() {
        return (
            <div>
                <Presentation
                {...this.state}
                handleChange={this.handleChange}
                />
            </div>
        )
    }
}

export default Container
