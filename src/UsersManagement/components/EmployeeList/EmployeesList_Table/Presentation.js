import React from "react"
//import MaterialTable from "material-table";
import { Link } from "react-router-dom"
import AddCircleOutlineRoundedIcon from "@material-ui/icons/AddCircleOutlineRounded";
import { Button } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
//import { CustomField } from "../../../../SharedComponents/FormFeilds";
//import Grid from "@material-ui/core/Grid";
//import InviteThroughExcel from "../../Settings/Masters/InviteThroughExcel";
//import CreateTwoToneIcon from "@material-ui/icons/CreateTwoTone";
//import VisibilityTwoToneIcon from "@material-ui/icons/VisibilityTwoTone";
//import DeleteOutlineTwoToneIcon from '@material-ui/icons/DeleteOutlineTwoTone';
import  CustomTable from '../../../../SharedComponents/MaterialTable'
import TableActions from '../../../../SharedComponents/TableActions'
import BulkUser_Creation from '../../../../Pages/BulkUser_Creation'
export default function Presentation(props) {
  const { employeelist } = props
 // const { data, handleChange, handleClickShowPassword, handleCreate } = props;
 
  console.log(props);
  const allEmployees = [];
  employeelist.length &&
  employeelist.forEach((employee)=>{
    console.log(employee)
    if(Object.keys(employee).length){
      allEmployees.push({ 
        name: employee.name,
        gender: employee.gender,
        email: employee.email,
        phone: employee.phone,
        employeecode: employee.uid,
        JobTitle: employee.JobTitle,
        department: employee.department,
        status: employee.status,
        Employeestatus: employee.Employeestatus,
        desgination: employee.desgination,
        qualification: employee.qualification,
        EmployStatus: employee.status
      })
      console.log(allEmployees)
    }
   
  })
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };


  
  const [edit, setEdit] = React.useState(false);

  const handleEdit = () => {
    setEdit(true);
  };
  const handleEditClose = () => {
    setEdit(false);
  };

  const [del, setDel] = React.useState(false);
  const handleDel = () => {
    setDel(true);
  };
  const handleDelClose = () => {
    setDel(false);
  };  
  
  
  return (
    <div>
      <div style={{position: "sticky",}}>
        <Button variant="contained" color="secondary" onClick={handleClickOpen}>
          <AddCircleOutlineRoundedIcon />
          New
        </Button>
      </div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Masters"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">

                {/* Bulk user creation */}
                <BulkUser_Creation/>

          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary">
            Close
          </Button>
        </DialogActions>
      </Dialog>

      <Dialog
        open={edit}
        onClose={handleEditClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Edit User"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {/* <Grid item xs={23} sm={16}>
              <CustomField
                type="text"
                name="FullName"
                handleChange={handleChange}
                label="fullName"
                required
                size="small"
                value={data.FullName}
                fullWidth
              />
            </Grid>
            <Grid item xs={23} sm={16}>
              <CustomField
                type="phone"
                name="phone"
                handleChange={handleChange}
                label="phone"
                required
                size="small"
                value={data.phone}
                fullWidth
              />
            </Grid>
            <Grid item xs={23} sm={16}>
              <CustomField
                type="select"
                label="Gender"
                name="gender"
                handleChange={handleChange}
                value={data.gender}
                required
                size="small"
                variant="outlined"
                fullWidth
                menuItems={["Male", "Female", "Others"]}
              />
            </Grid>
            <Grid item xs={23} sm={16}>
                  <CustomField
                    type="select"
                    label="Department"
                    name="department"
                    handleChange={handleChange}
                    value={data.department}
                    required
                    size="small"
                    variant="outlined"
                    fullWidth
                    menuItems={["ECE", "CSE", "IT", "ME", "CE", "EEE", "MBA"]}
                  />
                  </Grid> */}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleEdit} color="primary">
            Conform
          </Button>
          <Button onClick={handleEditClose} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>

      {/* Delete User */}
      
      <Dialog
        open={del}
        onClose={handleDelClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Delete User"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Are you want to Delete User Permanently
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDel} color="primary">
             Agree
          </Button>
          <Button onClick={handleDelClose} color="primary" autoFocus>
           DisAgree
          </Button>
        </DialogActions>
      </Dialog>

    <CustomTable
      title=""
      data={allEmployees}
      columns={[
        {
          title: "NAME",
          field: "name",
          dataType: "String"
        },
        {
          title: "GENDER",
          field: "gender",
          dataType: "String",
        },
        {
          title: "EMAIL",
          field: "email",
          dataType: "string",
        },
        {
          title: "Phone",
          field: "phone",
          dataType: "String"
        },
        {
          title: "DEPARTMENT",
          field: "department",
          dataType: "String",
        },
        {
          title: "DESGINATION",
          field: "desgination",
          dataType: "string",
        },
        {
          title: "QUALIFICATION",
          field: "qualification",
          dataType: "String",

        },
        {
          title: "Employee-Code",
          field: "employeecode",
          defaultSort: "asc",
          render: (rowData) => {
            return (
              <Link 
                style={{ color: "blue" }}
                to={"/users/employeeprofile"} >
                {rowData.employeecode}
              </Link>
            )
          },
          dataType: "String"
        },
      
        {
          title: "Status",
          field: "status",
          dataType: "String",
          render: (rowData) => {
            if (rowData.status === "Active")
              return (
                <span
                  className="chip chip-success"
                >Active</span>
              );
            else if (rowData.status === "Inactive")
              return (
                <span
                  className="chip chip-pending"
                >{rowData.status}</span>
              );
              else 
              return (
                <span className = "chip chip-rejected">{rowData.status}</span>
              )
          },
          customFilterAndSearch: (value, rowData) => {
            if (value.length === 0) return true;
            return value.includes(rowData.status.toString());
          },
        },
        {
          title: "Actions",
          field: "",
          render: (rowData) => {
            return (
              <div style={{ display: "flex" }}>
                <TableActions/>
              </div>
            );
          },
        },
      ]}
    />
    </div>
  )
}
