import React from "react";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Badge from "@material-ui/core/Badge";
import EmployeesList from "../EmployeesList_Table";
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-force-tabpanel-${index}`}
      aria-labelledby={`scrollable-force-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-force-tab-${index}`,
    "aria-controls": `scrollable-force-tabpanel-${index}`,
  };
}

// const useStyles = makeStyles((theme) => ({
//   root: {
//     backgroundColor: theme.palette.background.paper,
//     width: 500,
//   },
// }));

export default function Presentation(props) {
  const { employeelist=[] } = props;
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  //const handleChangeIndex = (index) => {
  // setValue(index);
  //};
console.log(employeelist)
  return (
    <div style={{ backgroundColor: "#fff" }}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
          aria-label="scrollable auto tabs example"
        >
          <Tab
            label={
              <Badge
                color="primary"
                children="All"
                max="10000"
                badgeContent={employeelist.length}
              />
            }
            {...a11yProps(0)}
          />
          <Tab
            label={
              <Badge
                color="primary"
                children="Active"
                max="10000"
                badgeContent={
                  employeelist.filter(
                    (employeelist) => employeelist.status === "Active"
                  ).length
                }
              />
            }
            {...a11yProps(1)}
          />
          <Tab
            label={
              <Badge
                color="secondary"
                children="Inactive"
                max="10000"
                badgeContent={
                  employeelist.filter(
                    (employeelist) => employeelist.status === "Inactive"
                  ).length
                }
              />
            }
            {...a11yProps(2)}
          />
          <Tab
            label={
              <Badge
                color="primary"
                children="Teaching"
                max="10000"
                badgeContent={
                  employeelist.filter(
                    (employeelist) => employeelist.status === "Teaching"
                  ).length
                }
              />
            }
            {...a11yProps(3)}
          />
          <Tab
            label={
              <Badge
                color="primary"
                children="Non-Teaching"
                max="10000"
                badgeContent={
                  employeelist.filter(
                    (employeelist) => employeelist.status === "Non-Teaching"
                  ).length
                }
              />
            }
            {...a11yProps(4)}
          />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <EmployeesList all={true} employeelist={employeelist} />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <EmployeesList
          active={true}
          handleSelect={props.handleSelect}
          employeelist={employeelist.filter(
            (employeelist) => employeelist.status === "Active"
          )}
        /> 
      </TabPanel>
      <TabPanel value={value} index={2}>
        <EmployeesList
          inactive={true}
          handleSelect={props.handleSelect}
          employeelist={employeelist.filter(
            (employeelist) => employeelist.status === "Inactive"
          )}
        />
      </TabPanel>
      <TabPanel value={value} index={3}>
        <EmployeesList
          Teaching={true}
          handleSelect={props.handleSelect}
          employeelist={employeelist.filter(
            (employeelist) => employeelist.status === "Teaching"
          )}
        />
      </TabPanel>
      <TabPanel value={value} index={4}>
        <EmployeesList
          Non-Teaching={true}
          handleSelect={props.handleSelect}
          employeelist={employeelist.filter(
            (employeelist) => employeelist.status === "Non-Teaching"
          )}
        />
      </TabPanel>
    </div>
  );
}
