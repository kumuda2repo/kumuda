import React, { Component } from "react";
import Presentation from "./Presentation";
import { _employeeList } from "../../../middleware/middleware";
import { connect } from "react-redux";

class Container extends Component {
  constructor(props) {
    super(props)
    console.log(props)
    this.state = {
      employeelist: props.employeelistUsers,
      role: "Employees"
    }
  }

  componentDidMount() {
    console.log(this.props)
    this.props.employeedetails(this.state.role)
  }

  componentWillReceiveProps(nextProps) {
    console.log(nextProps)
    this.setState({
      employeelist: nextProps.employeelistUsers
    })
  }

  render() {
    return (
      <div>
        <Presentation {...this.state} />
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    employeelistUsers: state.usermanagement.employeelistUsers
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    employeedetails: (role) => {
      dispatch(_employeeList(role))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Container);

