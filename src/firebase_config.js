import  firebase from "firebase/app";
import "firebase/auth";
import "firebase/storage"

const firebaseConfig = {
    apiKey: "AIzaSyASKUB8mXG8snbt2soJsbSJ_rGCuBlMEr4",
    authDomain: "kumuda-test.firebaseapp.com",
    databaseURL: "https://kumuda-test.firebaseio.com",
    projectId: "kumuda-test",
    storageBucket: "kumuda-test.appspot.com",
    messagingSenderId: "1029769689331",
    appId: "1:1029769689331:web:566cc181af53c057649f86",
    measurementId: "G-335P20HVMY"
};

const app = firebase.initializeApp(firebaseConfig);

const storage=firebase.storage();

export  { storage,app as default }
