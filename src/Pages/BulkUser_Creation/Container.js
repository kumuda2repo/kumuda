import React, { Component } from "react";
import { _singleUser } from "../../UsersManagement/middleware/middleware";
import Presentation from "./Presentation";
import { connect } from "react-redux";
class Container extends Component {
  constructor(props) {
    super(props);
    console.log(props, "9");
    this.state = {
      user: {
        FullName: "",
        gender: "",
        user: "",
        department: "",
        role: "",
        phone: "",
        uid: "",
        displayName: "",
        email: "",
        password: "",
        showPassword: false,
      },
      auth_create: {
        email: "",
        emailVerified: false,
        password: "",
        displayName: "",
        uid: "",
        disabled: false,
      },
    };
  }
  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
      auth_create: {
        ...this.state.auth_create,
        displayName: e.target.value,
        email: e.target.value,
        uid: e.target.value,
      },
    });
  };
  handleCreate = (e) => {
    const history_payload = {
      date: new Date().toISOString(),
      actionOn: this.state.user.email,
      action: `${this.state.user.email} is Successfully Created`,
      actionBy: this.props.loggedIn_UserEmail,
    };
    this.props.singleUser(
      this.state.user,
      this.state.auth_create,
      history_payload
    );
    this.setState({
      user: {
        FullName: "",
        gender: "",
        user: "",
        department: "",
        role: "",
        phone: "",
        uid: "",
        displayName: "",
        email: "",
      },
      auth_create: {
        email: "",
        emailVerified: false,
        password: "",
        displayName: "",
        uid: "",
        disabled: false,
      },
    });
  };

  render() {
    return (
      <div>
        <Presentation
          studentlist={this.props.studentlist}
          data={this.state}
          handleChange={this.handleChange}
          handleClickShowPassword={this.handleClickShowPassword}
          handleCreate={this.handleCreate}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    loggedIn_UserEmail: state.auth.user.email,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    singleUser: (user, auth_create, history_payload) => {
      dispatch(_singleUser(user, auth_create, history_payload));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
