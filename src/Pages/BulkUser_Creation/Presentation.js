import React from "react";
import Grid from "@material-ui/core/Grid";
import { CustomField } from "../../SharedComponents/FormFeilds";
import { Button } from "@material-ui/core";
import InviteThroughExcel from "../../UsersManagement/components/Settings/Masters/InviteThroughExcel";
function Presentation(props) {
  const { data, handleChange, handleClickShowPassword, handleCreate } = props;

  return (
    <div>
      <div style={{ marginTop: "10px" }}>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            {/*src\SharedComponents\FormFeilds.js */}
            <CustomField
              type="text"
              name="FullName"
              handleChange={handleChange}
              label="fullName"
              required
              size="small"
              value={data.FullName}
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <CustomField
              type="select"
              label="Gender"
              name="gender"
              handleChange={handleChange}
              value={data.gender}
              required
              size="small"
              variant="outlined"
              fullWidth
              menuItems={["Male", "Female", "Others"]}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <CustomField
              type="email"
              name="email"
              handleChange={handleChange}
              label="email"
              required
              size="small"
              value={data.email}
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <CustomField
              type="select"
              label="Department"
              name="department"
              handleChange={handleChange}
              value={data.department}
              required
              size="small"
              variant="outlined"
              fullWidth
              menuItems={["ECE", "CSE", "IT", "ME", "CE", "EEE", "MBA"]}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <CustomField
              type="select"
              label="Role"
              name="role"
              handleChange={handleChange}
              value={data.role}
              required
              size="small"
              variant="outlined"
              fullWidth
              menuItems={["Students", "Employee"]}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <CustomField
              type="phone"
              name="phone"
              handleChange={handleChange}
              label="phone"
              required
              size="small"
              value={data.phone}
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <CustomField
              type="alphanum"
              name="uid"
              handleChange={handleChange}
              label="REG-No"
              required
              size="small"
              value={data.uid}
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <CustomField
              type="password"
              name="password"
              label="Password"
              value={data.password}
              handleChange={handleChange}
              variant="outlined"
              fullWidth
              size="small"
              handleClickshowPassword={handleClickShowPassword}
            />
          </Grid>
        </Grid>
        <br />
        <div>
          <span>
            <InviteThroughExcel />
          </span>
        </div>
        <div style={{ float: "right" }}>
          <Button
            id="singleuser_create"
            onClick={handleCreate}
            disabled={
              !(
                data.FullName &&
                data.gender &&
                data.email &&
                data.department &&
                data.role &&
                data.phone &&
                data.uid &&
                data.password
              )
            }
            variant="contained"
            color="secondary"
          >
            Create
          </Button>
        </div>
      </div>
    </div>
  );
}

export default Presentation;
