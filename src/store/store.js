import { createStore, applyMiddleware, combineReducers } from "redux";
import thunk from "redux-thunk";
import authReducer from "../Authentication/reducers/authReducer";
import dashboardReducer from "../Dashboard/reducers/dashboardReducer";
import usermanagementreducer from "../UsersManagement/reducers/usermanagementreducer";
import { composeWithDevTools } from "redux-devtools-extension";
import ACTION from "../Authentication/actions/actionTypes";

const reducers = combineReducers({
  auth: authReducer,
  dashboard: dashboardReducer,
  usermanagement: usermanagementreducer,
});

const rootReducer = (state, action) => {
  // console.log(action);
  if (action.type === ACTION.LOGOUT) {
    state = undefined;
  }
  return reducers(state, action);
};

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
);

export default store;
