import React from "react";
import { Route, Switch } from "react-router-dom";
import Admissions from "./Admissions/components/StudentsList";
import DashboardView from "./Dashboard/components/DashboardView";
import SchedulingView from "./Scheduling/components/SchedulingView";
import SchedulingSettings from "./Scheduling/components/Settings";
import StudentList_navbar from "./UsersManagement/components/StudentsList/StudentsList_navbar";
import UsersSettings from "./UsersManagement/components/Settings/Index";
import EmployeeList_NavBar from "./UsersManagement/components/EmployeeList/EmployeeList_NavBar";
import History from "./UsersManagement/components/History";
import ProfileEmployee from "./UsersManagement/components/EmployeeList/EmployProfile/ProfileEmployee";
import Reports from './UsersManagement/components/Reports'
function Routes() {
  return (
    <Switch>
      <Route exact path="/" component={DashboardView} />
      <Route exact path="/scheduling" component={SchedulingView} />
      <Route exact path="/scheduling/settings" component={SchedulingSettings} />
      <Route exact path="/admissions" component={Admissions} />
      <Route exact path="/users/studentslist" component={StudentList_navbar} />
      <Route exact path="/users/settings" component={UsersSettings} />
      <Route
        exact
        path="/users/employeeslist"
        component={EmployeeList_NavBar}
      />
      <Route exact path="/users/history" component={History} />
      <Route exact path="/users/reports" component={Reports} />
      <Route exact path="/users/employeeprofile" component={ProfileEmployee} />
    </Switch>
  );
}

export default Routes;
