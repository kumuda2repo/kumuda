import React from "react"
import MaterialTable, { MTableToolbar } from "material-table"

function CustomTable({ title,data, columns, ToolBar, isToolBar, isLoading }) {
  return (
    <div>
      <MaterialTable
        title={title}
        data={data}
        columns={columns}
        isLoading={isLoading}
        options={{
          pageSize: 5,
          paginationPosition: "top",
           paginationType: "normal",
          filtering: true,
          selection: true,
          columnsButton: true,
          exportButton: true,
        }}
        components={{
          Toolbar: (props) => {
            return (
              <div>
                <MTableToolbar {...props} />
                <div style={{ padding: "0 10px" }}>
                  {isToolBar ? <ToolBar /> : null}
                </div>
              </div>
            )
          },
        }}
      />
    </div>
  )
}

export default CustomTable