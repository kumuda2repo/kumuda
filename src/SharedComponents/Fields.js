import React from "react";
import { IconButton, Grid } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import DateFnsUtils from "@date-io/date-fns";
import { red } from "@material-ui/core/colors";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";

export const textField = (
  data,
  label,
  name,
  required,
  section,
  isEditing,
  handleDelete,
  handleChange
) => {
  // console.log(data)
  return (
    <Grid item xs={6} className="d-flex">
      <TextField
        fullWidth
        label={label}
        name={name}
        value={data[name]}
        required={required}
        variant="outlined"
        size="small"
        onChange={(e) => handleChange(name, e.target.value)}
      />
      {isEditing ? (
        <IconButton
          onClick={() => {
            handleDelete(name, section);
          }}
        >
          <DeleteIcon style={{ color: red[500] }} />
        </IconButton>
      ) : null}
    </Grid>
  );
};

export const emailField = (
  data,
  label,
  name,
  required,
  section,
  isEditing,
  handleDelete,
  handleChange
) => {
  return (
    <Grid item xs={6} className="d-flex">
      <TextField
        fullWidth
        label={label}
        name={name}
        required={required}
        value={data[name]}
        type="email"
        variant="outlined"
        size="small"
        onChange={(e) => handleChange(name, e.target.value)}
      />
      {isEditing ? (
        <IconButton
          onClick={() => {
            handleDelete(name, section);
          }}
        >
          <DeleteIcon style={{ color: red[500] }} />
        </IconButton>
      ) : null}
    </Grid>
  );
};

export const phoneField = (
  data,
  label,
  name,
  required,
  section,
  isEditing,
  handleDelete,
  handleChange
) => {
  return (
    <Grid item xs={6} className="d-flex">
      <TextField
        fullWidth
        label={label}
        name={name}
        required={required}
        value={data[name]}
        type="number"
        variant="outlined"
        size="small"
        onChange={(e) => handleChange(name, e.target.value)}
      />
      {isEditing ? (
        <IconButton
          onClick={() => {
            handleDelete(name, section);
          }}
        >
          <DeleteIcon style={{ color: red[500] }} />
        </IconButton>
      ) : null}
    </Grid>
  );
};

export const selectField = (
  data,
  label,
  name,
  required,
  values,
  section,
  isEditing,
  handleDelete,
  handleChange
) => {
  console.log(values);
  return (
    <Grid item xs={6} className="d-flex">
      <TextField
        fullWidth
        select
        label={label}
        name={name}
        required={required}
        value={data[name]}
        onChange={(e) => handleChange(name, e.target.value)}
        size="small"
        variant="outlined"
      >
        <MenuItem key="" value="">
          None
        </MenuItem>
        {values.map((option) => (
          <MenuItem key={option.id} value={option.id}>
            {option.item}
          </MenuItem>
        ))}
      </TextField>
      {isEditing ? (
        <IconButton
          onClick={() => {
            handleDelete(name, section);
          }}
        >
          <DeleteIcon style={{ color: red[500] }} />
        </IconButton>
      ) : null}
    </Grid>
  );
};

export const addressFields = (
  data,
  label,
  name,
  required,
  section,
  isEditing,
  handleDelete,
  handleChange
) => {
  return (
    <Grid item xs={6} className="d-flex">
      <TextField
        fullWidth
        label={label}
        name={name}
        required={required}
        value={data[name]}
        variant="outlined"
        size="small"
        onChange={(e) => handleChange(name, e.target.value)}
      />
      {isEditing ? (
        <IconButton
          onClick={() => {
            handleDelete(name, section);
          }}
        >
          <DeleteIcon style={{ color: red[500] }} />
        </IconButton>
      ) : null}
    </Grid>
  );
};

export const dateField = (
  data,
  label,
  name,
  required,
  section,
  isEditing,
  handleDelete,
  handleChange
) => {
  console.log(data);
  return (
    <Grid item xs={6} className="d-flex">
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <KeyboardDatePicker
          allowKeyboardControl={true}
          label={label}
          format="MM/dd/yyyy"
          name={name}
          value={data[name] ? new Date(data[name]) : null}
          required={required}
          onChange={(date) => {
            if (!isNaN(Date.parse(date)))
              handleChange(name, new Date(date).toISOString());
          }}
          fullWidth
          KeyboardButtonProps={{
            "aria-label": "change date",
          }}
        />
      </MuiPickersUtilsProvider>
      {isEditing ? (
        <IconButton
          onClick={() => {
            handleDelete(name, section);
          }}
        >
          <DeleteIcon style={{ color: red[500] }} />
        </IconButton>
      ) : null}
    </Grid>
  );
};
