import React from 'react';
import Prompt from './Snackbar'
import HoldPrompt from './ProgressSnackbar'
import Backdrop from "./Backdrop"
import { render } from 'react-dom';

export const waitingMsg = (content) => {
  return render(
    <HoldPrompt open={true} content={content} severity='info' />,
    document.getElementById('notifications-hold-box')
  )
}

export const stopWaitMsg = () => {
  return render(<div></div>, document.getElementById('notifications-hold-box'))
}

export const successMsg = (content) => {
  return render(
    <Prompt open={true} content={content} severity='success' />,
    document.getElementById('notifications-box')
  )
}

export const errorMsg = (content) => {
  return render(
    <Prompt open={true} content={content} severity='error' />,
    document.getElementById('notifications-box')
  )
}

export const backDrop = (condition) => {
  return render(
    <Backdrop openBackdrop={condition} />,
    document.getElementById('backdrop-loading')
  )
}
