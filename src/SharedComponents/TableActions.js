import React from "react";
import CreateTwoToneIcon from "@material-ui/icons/CreateTwoTone";
import VisibilityTwoToneIcon from "@material-ui/icons/VisibilityTwoTone";
import DeleteOutlineTwoToneIcon from "@material-ui/icons/DeleteOutlineTwoTone";
import { Button } from "@material-ui/core";

function TableActions() {
  const [setEdit] = React.useState(false);

  const handleEdit = () => {
    setEdit(true);
  };
  

  const [setDel] = React.useState(false);
  const handleDel = () => {
    setDel(true);
  };
  

  return (
    <div style={{ display: "flex" }}>
      <Button variant="outlined" color="primary">
        <VisibilityTwoToneIcon />
      </Button>
      <Button variant="outlined" color="primary" onClick={handleEdit}>
        <CreateTwoToneIcon />
      </Button>

      <Button variant="outlined" color="primary" onClick={handleDel} >
        <DeleteOutlineTwoToneIcon />
      </Button>
    </div>
  );
}

export default TableActions;
