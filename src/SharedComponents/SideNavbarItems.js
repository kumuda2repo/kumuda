import React from "react"
import PersonTwoToneIcon from '@material-ui/icons/PersonTwoTone';
import { VscFileSubmodule } from "react-icons/vsc";
import SettingsApplicationsTwoToneIcon from '@material-ui/icons/SettingsApplicationsTwoTone';
import ReportTwoToneIcon from '@material-ui/icons/ReportTwoTone';
import RestorePageTwoToneIcon from '@material-ui/icons/RestorePageTwoTone';
import SupervisedUserCircleTwoToneIcon from '@material-ui/icons/SupervisedUserCircleTwoTone';
let scheduling = [
    {
        itemText: "Automated",
        itemIcon: <VscFileSubmodule />,
    },
    {
        itemText: "Manual",
        itemIcon: <VscFileSubmodule />,
    },
    {
        itemText: "Schedule views",
        itemIcon: <VscFileSubmodule />,
    },
    {
        itemText: "All Schedules",
        itemIcon: <VscFileSubmodule />,
    },
    {
        itemText: "Settings",
        itemIcon: <SettingsApplicationsTwoToneIcon />,
        link: "/scheduling/settings",
    },
    {
        itemText: "Reports",
        itemIcon: <ReportTwoToneIcon/>,
    },
]

let users = [
    {
        itemText: "Students",
        itemIcon: <PersonTwoToneIcon />,
        link: "/users/studentslist",
    },
    {
        itemText: "Employees",
        itemIcon: <SupervisedUserCircleTwoToneIcon />,
        link: "/users/employeeslist",
    },
    {
        itemText: "Settings",
        itemIcon: <SettingsApplicationsTwoToneIcon />,
        link: "/users/settings",
    },
    {
        itemText: "History",
        itemIcon: <RestorePageTwoToneIcon/>,
        link: "/users/history",
    },
    {
        itemText: "Reports",
        itemIcon: <ReportTwoToneIcon />,
        link :"/users/reports"
    },
]

export const SideNavbarItems = [
    { name: "scheduling", sideBarItems: scheduling },
    { name: "users", sideBarItems: users },
]