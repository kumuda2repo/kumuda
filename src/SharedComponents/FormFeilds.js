import React, { useState } from "react";
import {
  TextField,
  MenuItem,
  FormControlLabel,
  Checkbox,
} from "@material-ui/core";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import {
  MobileNumberFormatter,
  NumberFormatCustom,
} from "./customNumberFormats";
import validate from "./validation";
import { InputAdornment, IconButton } from "@material-ui/core";
import { Visibility, VisibilityOff } from "@material-ui/icons";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
export const TextInput = ({
  label,
  name,
  handleChange,
  required,
  value = "",
  variant = "outlined",
}) => {
  return (
    <TextField
      label={label}
      name={name}
      onChange={handleChange}
      required={required}
      value={value}
      variant={variant}
      size="small"
      fullWidth
    />
  );
};
export const NameInput = ({
  label,
  name,
  handleChange,
  required,
  value = "",
  variant = "outlined",
}) => {
  return (
    <TextField
      label={label}
      name={name}
      onChange={(e) =>
        validate.checkName(e.target.value) ? handleChange(e) : () => {}
      }
      required={required}
      helperText={!validate.checkName(value) ? "Enter valid " + label : ""}
      value={value}
      variant={variant}
      size="small"
      fullWidth
    />
  );
};
export const PhoneInput = ({
  label,
  name,
  handleChange,
  required,
  value = "",
  variant = "outlined",
}) => {
  return (
    <TextField
      label={label}
      name={name}
      onChange={handleChange}
      helperText={!validate.checkNumber(value) ? "Enter valid " + label : ""}
      variant={variant}
      size="small"
      required={required}
      value={value}
      fullWidth
      InputProps={{
        inputComponent: MobileNumberFormatter,
      }}
    />
  );
};
export const EmailInput = ({
  label,
  name,
  handleChange,
  className,
  required,
  value = "",
  variant = "outlined",
}) => {
  return (
    <ValidatorForm autoComplete="off">
    <TextValidator
      label={label}
      name={name}
      className={className}
      type="email"
      onChange={handleChange}
      helperText={!validate.checkEmail(value) ? "Enter valid " + label : ""}
      errorMessages={["this field is required", "email is not valid"]}
      validators={["required", "isEmail"]}
      required={required}
      value={value}
      variant={variant}
      size="small"
      fullWidth
    />
    </ValidatorForm>
  );
};
export const NumberInput = ({
  label,
  name,
  handleChange,
  required,
  value = "",
  variant = "outlined",
}) => {
  return (
    <TextField
      label={label}
      name={name}
      onChange={handleChange}
      variant={variant}
      size="small"
      required={required}
      value={value}
      fullWidth
      InputProps={{
        inputComponent: NumberFormatCustom,
      }}
    />
  );
};
export const SelectInput = ({
  label,
  name,
  handleChange,
  handleRole,
  required,
  menuItems=[],
  value ="",
  variant = "outlined",
}) => {
  return (
    <TextField
      select
      label={label}
      name={name}
      onChange={handleChange}
      variant={variant}
      size="small"
      required={required}
      value={value}
      fullWidth
    >
      {menuItems.map((item) => (
        <MenuItem value={item}>{item}</MenuItem>
      ))}
    </TextField>
  );
};
export const PasswordInput = ({
  label,
  name,
  handleChange,
  className,
  required,
  value = "",
  variant = "outlined",
}) => {
  const [showPassword, setShowPassword] = useState(false);
  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };
  return (
    <ValidatorForm autoComplete="off">
    <TextValidator
      password
      label={label}
      name={name}
      className={className}
      type={showPassword ? "text" : "password"}
      onChange={handleChange}
      variant={variant}
      size="small"
      required={required}
      value={value}
      fullWidth
      InputProps={{
        endAdornment: (
          <InputAdornment position="end">
            <IconButton onClick={handleClickShowPassword}>
              {showPassword ? <VisibilityOff /> : <Visibility />}
            </IconButton>
          </InputAdornment>
        ),
      }}
    />
    </ValidatorForm>
  );
};
export const DateInput = ({
  label,
  name,
  handleChange,
  required,
  value,
  maxDate = undefined,
  minDate = undefined,
}) => {
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <KeyboardDatePicker
        required={required}
        size="small"
        label={label}
        format="MM/dd/yyyy"
        maxDate={maxDate}
        minDate={minDate}
        onChange={(date) => handleChange({ target: { value: date } })}
        value={value ? value : null}
        fullWidth
        name={name}
        KeyboardButtonProps={{
          "aria-label": "change date",
        }}
      />
    </MuiPickersUtilsProvider>
  );
};

const CheckBoxInput = ({
  label,
  name,
  handleChange,
  required,
  value = false,
}) => {
  return (
    <FormControlLabel
      control={
        <Checkbox
          checked={value}
          required={required}
          onChange={(e) =>
            handleChange({ ...e,target: { value: e.target.checked,name:e.target.name } })
          }
          
        />
      }
      label={label}
      name={name}
    />
  );
};

const buttonInput =()=>{
  
}

export function CustomField(details) {
  switch (details.type) {
    case "text":
      return <TextInput {...details} />;

    case "name":
      return <NameInput {...details} />;

    case "phone":
      return <PhoneInput {...details} />;

    case "email":
      return <EmailInput {...details} />;

    case "number":
      return <NumberInput {...details} />;

    case "checkbox":
      return <CheckBoxInput {...details} />;

    case "select":
      return <SelectInput {...details} />;

    case "password":
      return <PasswordInput {...details} />;

    case "date":
      return <DateInput {...details} />;

    case "address":
      return <TextInput {...details} />;

    case "alphanum":
      return <TextInput {...details} />;

    case "fromdate":
      return <DateInput {...details} />;

    case "todate":
      return <DateInput {...details} />;

    default:
      return <div />;
  }
}
