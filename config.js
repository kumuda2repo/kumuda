const dev = {
    SERVER_URL: 'http://localhost:5000/kumuda-test/us-central1/api',
}

const prod = {
    SERVER_URL: 'http://localhost:5000/kumuda-test/us-central1/api',
}

const is_dev = process.env.NODE_ENV.toLowerCase() === 'development';

module.exports = is_dev ? dev : prod